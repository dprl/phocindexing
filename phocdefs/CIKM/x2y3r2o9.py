
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x2y3r2o9() -> PhocCompose:
    x=2
    y=3
    r=2
    o=9    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 1))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 2))
    if r>1:
        to_compose.append(RectanglePhoc(1))
    if o>1:
        to_compose.append(EllipsisPhoc(8))
    compose = PhocCompose(to_compose)

    return compose
                