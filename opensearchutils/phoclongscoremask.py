mask = {
    "script": {
        "lang": "painless",
        "source":
            """
            double totalBitCount = 0;
            java.util.ArrayList mask = params['mask'];
            String maskOp = params['mask_op'];
            for (String tokenKey : params['tokens']) {
                long i = params[tokenKey];
                 if (doc.containsKey(tokenKey) && doc[tokenKey].size()>0) {
                    Long longIntDoc = doc[tokenKey].get(0);
                    long docInt = longIntDoc.longValue();
                    long masked = docInt & i;
                    String strIntArr = Long.toBinaryString(masked);
                    double sum = 0;
                    if (maskOp.equals('mul')) {
                        for (int c=strIntArr.length()-1; c>=0; c--) {
                            sum += Double.parseDouble(\"\"+strIntArr.charAt(c)) * mask.get(c);
                        }
                    } else if (maskOp.equals('add')) {
                        for (int c=strIntArr.length()-1; c>=0; c--) {
                            sum += Double.parseDouble(\"\"+strIntArr.charAt(c)) + mask.get(c);
                        }
                    }
                    totalBitCount += sum;
                }
            }
            double norm_factor = doc['b_norm_factor'].getValue().doubleValue();
            return (double) totalBitCount/(double) norm_factor;
        """
    }
}
