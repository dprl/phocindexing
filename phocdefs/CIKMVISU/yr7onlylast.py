import os

from anyphoc.canvas.converters.svgcanvas import svg_to_svg_canvas
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x0y7r7o0_only_last() -> PhocCompose:

    to_compose = [
        HorzLinePhoc(0, 6, starting_level=6),
        RectanglePhoc(6, starting_level=6)
    ]

    compose = PhocCompose(to_compose, skip_first_level=True)

    return compose


if __name__ == "__main__":
    print(get_x0y7r7o0_only_last().explain())

    with open(os.path.join("phocdefs", "CIKMVISU", "h_dom.svg")) as f:
        svg = f.read()

    canvas_ = svg_to_svg_canvas(svg, region_type="line")
    get_x0y7r7o0_only_last().build_html(canvas_, os.path.join("phocdefs", "CIKMVISU"), "ok")