
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x5y3r6o1() -> PhocCompose:
    x=5
    y=3
    r=6
    o=1    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 4))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 2))
    if r>1:
        to_compose.append(RectanglePhoc(5))
    if o>1:
        to_compose.append(EllipsisPhoc(0))
    compose = PhocCompose(to_compose)

    return compose
                