
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.phoccompose import PhocCompose


def get_r10() -> PhocCompose:  

    rect_phoc = RectanglePhoc(9)
    compose = PhocCompose([
        rect_phoc
    ])

    return compose
                
