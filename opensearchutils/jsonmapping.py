from typing import Set
import json
import logging


def build_json_mapping(token_set: Set, vector_length: int, idf_map={}):
    
    if 0 < vector_length <= 8:
        opensearch_type = "byte"
    elif 8 < vector_length <= 16:
        opensearch_type = "short"
    elif 16 < vector_length <= 32:
        opensearch_type = "integer"
    elif 32 < vector_length <= 64:
        opensearch_type = "long"
    else:        
        opensearch_type = "keyword"

    json_dict = {
            "mappings":
            {
                "properties":
                {}
            }
    }
    properties = json_dict["mappings"]["properties"]
    for token in token_set:
        if token != '.':
            properties[token] = {
                    "type": opensearch_type,
                    "enabled": True
                    }
    for token in idf_map:
        if token != '.':
            token_key = token + '_idf'
            properties[token_key] = {
                "type": opensearch_type,
                "enabled": False
            }
    properties["post_id"] = {
        "type": "long",
        "index": True
    }
    properties["b_norm_factor"] = {
        "type": "long",
        "index": True,
        "enabled": False
    }

    return json.dumps(json_dict, ensure_ascii=False).encode('utf-8')
