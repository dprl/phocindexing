from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_o9() -> PhocCompose:

    ellip_phoc = EllipsisPhoc(8)
    compose = PhocCompose([
        ellip_phoc
        ])
    return compose
