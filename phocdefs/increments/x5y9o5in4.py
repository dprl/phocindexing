
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x5y9o5in4() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 5, increments=4)
    horz_phoc = HorzLinePhoc(0, 9, increments=4)
    ellip_phoc = EllipsisPhoc(5, increments=4)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
        ])
    return compose
    