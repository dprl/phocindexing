import pandas as pd
import os
from pandas import DataFrame


if __name__ == "__main__":

    results_dataframe = pd.read_csv(os.path.join("grid_analysis", "all_results.tsv"), sep='\t', header=0)
    ndcg_frame: DataFrame = results_dataframe.loc[:, ["System", "nDCG'"]]
    p10_frame: DataFrame = results_dataframe.loc[:, ["System", "p@10"]]

    ndcg_frame_sorted = \
        ndcg_frame.sort_values(by="nDCG'", ascending=False).drop_duplicates("System").reset_index(drop=True)
    p10_frame_sorted = \
        p10_frame.sort_values(by="p@10", ascending=False).drop_duplicates("System").reset_index(drop=True)

    ndcg_frame_sorted['rank'] = ndcg_frame_sorted.index
    p10_frame_sorted['rank'] = p10_frame_sorted.index

    # get RR, need to add 1 to account for 0-indexing
    ndcg_frame_sorted['rr'] = ndcg_frame_sorted['rank'].apply(lambda x: 1/(x+1))
    p10_frame_sorted['rr'] = p10_frame_sorted['rank'].apply(lambda x: 1/(x+1))

    combined: DataFrame = p10_frame_sorted.copy().drop(["p@10"], axis=1)
    combined["rr_sum"] = 0.0

    for system in list(results_dataframe["System"]):
        ndcg_rr = max(ndcg_frame_sorted.loc[ndcg_frame_sorted["System"] == system]["rr"])
        p10_rr = max(p10_frame_sorted.loc[p10_frame_sorted["System"] == system]['rr'])

        combined.loc[combined["System"] == system, 'rr_sum'] = ndcg_rr + p10_rr

    combined = combined.sort_values(by="rr_sum", ascending=False).reset_index(drop=True)
    print("Top combined scores")
    print(combined.head())
    print("Top p10 scores")
    print(p10_frame_sorted.head())
    print("Top ndcg scores")
    print(ndcg_frame_sorted.head())
