from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def build_compose(x, y, r, o) -> PhocCompose:
    to_compose = []
    if x > 0:
        to_compose.append(VertLinePhoc(0, x - 1))
    if y > 0:
        to_compose.append(HorzLinePhoc(0, y - 1))
    if r > 0:
        to_compose.append(RectanglePhoc(r - 1))
    if o > 0:
        to_compose.append(EllipsisPhoc(o - 1))
    compose = PhocCompose(to_compose)

    return compose
