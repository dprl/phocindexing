from typing import Dict, List
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from anyphoc.indexutils.compressbitvectors import compress_bitvectors
from opensearchutils.idf import filter_idf_map_for_bulk
import json
import ctypes
import math


def get_norm_factor(x: Dict) -> float:    
    sum_of_sums = 0
    unified_symbols = x['__unifiedkeys__']
    for k in x.keys():
        if k == 'b_norm_factor' or k == 'post_id':
            continue
        if k not in unified_symbols and k != '__unifiedkeys__':
            if isinstance(x[k], list):
                sum_of_sums += sum([i > 0 for i in x[k]])
            else:
                sum_of_sums += int(x[k] > 0)

    return math.sqrt(sum_of_sums)


def build_batch_insert_inner(x, idx, pid, index_name, index_type, idf_map) -> str:
    try:        
        compressed = compress_bitvectors(x, index_type)
        compressed["b_norm_factor"] = get_norm_factor(x)
        compressed["post_id"] = pid

        if idf_map is not None:
            filtered_idf_map = filter_idf_map_for_bulk(idf_map, compressed)
            compressed.update(filtered_idf_map)

        compressed_json = json.dumps(compressed, ensure_ascii=False)

        return """{{ "index" : {{ "_index": "{index_name}", "_id" : "{idx}" }} }}
{mapjson}""".format(idx=idx, mapjson=compressed_json, index_name=index_name)
    except Exception as e:
        print(f'1failed to build json id {idx} vectors {x}')
        print(e)
        return ""


def build_batch_insert(index_name, vector_length, idf_map=None) -> udf:
    if 0 < vector_length <= 8:
        index_type = ctypes.c_int8
    elif 8 < vector_length <= 16:
        index_type = ctypes.c_int16
    elif 16 < vector_length <= 32:
        index_type = ctypes.c_int32
    elif 32 < vector_length <= 64:
        index_type = ctypes.c_int64
    else:
        index_type = "hex"
    return udf(lambda x, idx, pid: build_batch_insert_inner(x, idx, pid, index_name, index_type, idf_map), StringType())
        
