import argparse
import time
from configparser import ConfigParser, ExtendedInterpolation
from typing import Dict
from opensearchpy import OpenSearch
import os
import glob
from io import StringIO
import json

from tqdm import tqdm


def get_data_from_file(file_name):
    with open(file_name, encoding="utf8", errors='ignore') as file:
        retval = []
        for line in file:
            if len(line.strip()) > 1:
                d = json.load(StringIO(line))
                if '.' in d:
                    d.pop('.')
                remove_inter_periods(d)

                retval.append(d)
        return retval


def remove_inter_periods(d: Dict) -> None:
    for key in list(d):

        if '.' in key:
            new_key = key.replace(".", "")
            d[new_key] = d[key]
            d.pop(key)


def load_file_as_gen(filepath):
    data = get_data_from_file(filepath)
    for i, doc in zip(data[0::2], data[1::2]):
        i['index']['_source'] = doc
        i['index']['_type'] = '_doc'
        yield i['index']


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="configs/xy5.ini")
    parsed_args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(parsed_args.config)

    input_dir = config.get('metrics', 'JSON_CHUNKS_DIR')
    retrieval_function = config.get('opensearch', 'RETRIEVAL_FUNC')
    index_name = config.get('opensearch', 'INDEX_NAME')
    mapping_file = config.get('spark', 'MAPPING_FILE')

    path = os.path.join(input_dir, '*.json')

    if retrieval_function == 'N/A':
        retrieval_function = ""

    index_name = index_name.lower()

    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': 'localhost', 'port': '9200'}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    # if client.indices.exists(index=index_name):
    #     client.indices.delete(index=index_name)
    if not client.indices.exists(index=index_name):
        client.indices.create(index=index_name, body={"settings": {"index": {
            "number_of_shards": 10,
            "number_of_replicas": 0,
            "refresh_interval": "120s"  # before 2s
        }}})
        client.indices.put_settings(
            index=index_name,
            body={
                "index.mapping.total_fields.limit": 2000
            }
        )
        if retrieval_function == "symbol-percentage":
            print(f"loading mapping file from {mapping_file}")
            mapping_data = get_data_from_file(mapping_file)[0]
            client.indices.put_mapping(index=index_name, body=mapping_data, request_timeout=10000)

    failures = 0
    files = glob.glob(path, recursive=True)
    print(f"loading index from {path}")
    data = []
    start = time.time()
    for f in tqdm(files):

        # print('current file', f)

        data.extend(get_data_from_file(f))
        if len(data) > 1000 * 30:
            res = client.bulk(index=index_name, body=data, request_timeout=10000)
            for s in res['items']:
                i = s['index']
                if i["status"] != 200 and i["status"] != 201:
                    failures += 1
                    print("Failure!")
                    print(i)
                elif i["result"] != "updated" and i["result"] != "created":
                    failures += 1
                    print("Failure!")
                    print(i)
            data = []
    # push any data that is left
    if len(data) > 0:
        res = client.bulk(index=index_name, body=data, request_timeout=10000)
        for s in res['items']:
            i = s['index']
            if i["status"] != 200 and i["status"] != 201:
                failures += 1
                print("Failure!")
                print(i)
            elif i["result"] != "updated" and i["result"] != "created":
                failures += 1
                print("Failure!")
                print(i)
    print(f"{failures} total failures")
    end = time.time()
    print("time taken to load bulk index files into OpenSearch in seconds: ", end - start)
