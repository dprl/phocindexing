import os
from pyspark.sql import SparkSession
import argparse

from pyspark.sql.functions import regexp_replace, col


def main(spark, args):
    tsv_df = spark.read\
        .option("inferSchema", True)\
        .option("header", True)\
        .option("mode", "DROPMALFORMED")\
        .option("multiLine",True)\
        .option("escape", "\"")\
        .csv(args.tsvdir, sep=r'\t')    

    tsv_df = tsv_df.dropDuplicates((["visual_id"]))    

    os.makedirs(args.outfile, exist_ok=True)

    tsv_df_clean = tsv_df \
        .withColumnRenamed("formula", "formulaOld") \
        .withColumn("formula", regexp_replace(col("formulaOld"), "[\n\r]", " ")) \
        .drop("formulaOld")

    tsv_df_clean.write\
        .mode('overwrite')\
        .option("multiLine", True)\
        .option("header", True)\
        .option("delimiter", "\t")\
        .option('encoding', 'utf-8')\
        .option("escape", "\"")\
        .csv(args.outfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--tsvdir")
    parser.add_argument('-o', "--outfile")

    parsed_args = parser.parse_args()
    spark_session = SparkSession.builder\
        .appName("anyphoc")\
        .master("local[*]")\
        .config("spark.driver.memory", "100g")\
        .config("spark.executor.memory", "100g")\
        .getOrCreate()
    main(spark_session, parsed_args)
