
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x0y8r1o4() -> PhocCompose:
    x=0
    y=8
    r=1
    o=4    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, -1))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 7))
    if r>1:
        to_compose.append(RectanglePhoc(0))
    if o>1:
        to_compose.append(EllipsisPhoc(3))
    compose = PhocCompose(to_compose)

    return compose
                