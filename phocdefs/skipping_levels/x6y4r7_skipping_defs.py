from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.phoccompose import PhocCompose

def get_x6y4r7_no_top() -> PhocCompose:
    horz_phoc = HorzLinePhoc(0,5)
    vert_phoc = VertLinePhoc(0,3)
    rect_phoc = RectanglePhoc(6)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        rect_phoc
    ], skip_first_level=True)
    return compose

def get_x6y4r7_skip2() -> PhocCompose:
    horz_phoc = HorzLinePhoc(0,5, starting_level=2)
    vert_phoc = VertLinePhoc(0,3, starting_level=2)
    rect_phoc = RectanglePhoc(6, starting_level=2)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        rect_phoc
    ], skip_first_level=True)
    return compose

def get_x6y4r7_odd() -> PhocCompose:
    horz_phoc = HorzLinePhoc(0,5, starting_level=2, skipping_step=2)
    vert_phoc = VertLinePhoc(0,3, starting_level=2, skipping_step=2)
    rect_phoc = RectanglePhoc(6, starting_level=2, skipping_step=2)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        rect_phoc
    ], skip_first_level=False)
    return compose


def get_x6y4r7_even() -> PhocCompose:
    horz_phoc = HorzLinePhoc(0,5, starting_level=1, skipping_step=2)
    vert_phoc = VertLinePhoc(0,3, starting_level=1, skipping_step=2)
    rect_phoc = RectanglePhoc(6, starting_level=1, skipping_step=2)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        rect_phoc
    ], skip_first_level=True)
    return compose

