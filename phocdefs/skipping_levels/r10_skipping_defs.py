from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.phoccompose import PhocCompose


def get_r10_no_top() -> PhocCompose:
    rect_phoc = RectanglePhoc(9)
    compose = PhocCompose([
        rect_phoc
    ], skip_first_level=True)
    return compose

def get_r10_skip2() -> PhocCompose:
    rect_phoc = RectanglePhoc(9, starting_level=2)
    compose = PhocCompose([
        rect_phoc
    ], skip_first_level=True)
    return compose

def get_r10_odd() -> PhocCompose:
    rect_phoc = RectanglePhoc(9, starting_level=2, skipping_step=2)
    compose = PhocCompose([
        rect_phoc
    ], skip_first_level=False)
    return compose


def get_r10_even() -> PhocCompose:
    rect_phoc = RectanglePhoc(9, starting_level=1, skipping_step=2)
    compose = PhocCompose([
        rect_phoc
    ], skip_first_level=True)
    return compose

