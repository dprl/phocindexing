# PHOC  Indexing

*Authors:* Matt Langsenkamp, Richard Zanibbi

Based on previous work done by Robin Avenoso

(Document and Pattern Recognition Lab (dprl), Rochester Institute of Technology, USA)

## Table of Contents

- [Installation](#installation)
- [Indexing Demo](#indexing-demo)

## Installation

This code was developed and tested on Ubuntu 20.04

Make sure you have Docker and docker-compose installed on your system. If not it can be downloaded from here: [get docker](https://docs.docker.com/get-docker/)

Once docker is installed make sure it is in swarm mode by running `docker swarm init`

Run `docker compose up`. This will run docker compose against the docker-compose.yml file in this directory. It will download the opensearch docker images and set up a small local cluster. This may take a minute of two. Go to `http://localhost:5601` in your browser and sign in with the username "admin" and the password "admin". If you are able to sign in then the installation was successful.
If you get an error or the nodes fail to start for whatever reason it may be because your `vm.max_map_count` is too low. Information on how to fix the issue can be found here: [Opensearch Settings Docs](https://opensearch.org/docs/latest/opensearch/install/important-settings/)

Make sure you have python and the Conda package manager installed. If not it can be downloaded from here: [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)

Next run `make`. It will install the anyphoc library as well as set up a conda env. 

Additionaly make sure java is installed. If it is not installed it can be on linux (debian) systems by running `sudo apt install default-jre`

## Running experiments
Running an experiment will perform the full pipeline of defining a phoc definition, starting Opensearch, creating (or recreating) an OpenSearch index,
starting up mathjax containers for svg generation, running phoc generation against the specified dataset, loading said phocs into the created opensearch index, 
and finally getting any and all the specified metrics back regarding this experiment. 

### Setting up an experiment
The specification for these experiments is managed solely by an `.ini` file stored in `/configs`. Most values specify endpoints, names, or file paths and will not be explained in detail.
Under the `[PHOC]` section there is and entry for `PHOC_DEF`. This points to the phoc definition specified in `phocdefs`. As an example we will register a new phoc definition
that tests for horizontal splits to a level of 5 and vertical splits to a level of 5. 

First write a function and store it in the `phocdefs` folder

```
fromanyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_xy5() -> phoccompose:

    vert_phoc = VertLinePhoc(0, 4)
    horz_phoc = HorzLinePhoc(0, 4)
    compose = phoccompose([
        vert_phoc,
        horz_phoc
        ])
    return compose 
```

Then in the `__init__.py` file in the `phocdefs` folder register it in the dictionary
```
from anyphocphoc.phoccompose import PhocCompose
from phocdefs.test import get_example
from phocdefs.xy5 import get_xy5
from typing import Callable, Dict


phoc_defs: Dict[str, Callable[[], PhocCompose]] = {
    "EXAMPLE": get_example,
    "XY5": get_xy5
}
```
Finally, you can add "XY5" as the phocdef in a new `.ini` file (configs/xy5.ini). Note that there are other parameters you can set.
```
./bin/runner ./configs/xy5.ini
```
The following command can take up to 25 minutes to run depending on your hardware. After the run is complete the path to an evaluation file will be printed to the console.
Run the following command to get the metrics for the run.
```
cat ./outputs/eval/xy5_example_results.tsv
```
### Note on SVG rendering
The very first time a specific `.ini` 
## Support

This material is based upon work supported by the National Science Foundation
(USA) under Grant Nos. IIS-1717997 (MathSeer), and 2019897 (MMLI), and the
Alfred P. Sloan Foundation under Grant No. G-2017-9827.
Any opinions, findings and conclusions or recommendations expressed in this
material are those of the author(s) and do not necessarily reflect the views of
the National Science Foundation or the Alfred P. Sloan Foundation.
