
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x4y0r4o1() -> PhocCompose:
    x=4
    y=0
    r=4
    o=1    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 3))
    if y>1:
        to_compose.append(HorzLinePhoc(0, -1))
    if r>1:
        to_compose.append(RectanglePhoc(3))
    if o>1:
        to_compose.append(EllipsisPhoc(0))
    compose = PhocCompose(to_compose)

    return compose
                