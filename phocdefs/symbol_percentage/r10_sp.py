from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.phoccompose import PhocCompose


def get_r10_sp() -> PhocCompose:
    rect_phoc = RectanglePhoc(9, percentages=True)
    compose = PhocCompose([
        rect_phoc
    ])
    return compose
