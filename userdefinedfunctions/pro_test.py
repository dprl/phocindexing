import logging
import traceback
from typing import Callable
from pyspark.sql.types import StringType, MapType, ArrayType, IntegerType
from pyspark.sql.functions import udf
from anyphoc.phoc.phoccompose import PhocCompose


def test_inner(canvas, phoc_def: Callable[[], PhocCompose], doc: str, page: int, region: int):
    try:        
        return phoc_def().test(canvas)
    except Exception as e:
        logger = logging.getLogger("broadcast")
        logger.warning(f"vector extraction failed against doc: {doc}, page {page}, region {region}"
                       f" caused by exception {e}")
        traceback.print_exc()
        return None


def pro_test(phoc_def: Callable[[None], PhocCompose]) -> udf:
    return udf(lambda canvas, doc, page, region:
               test_inner(canvas, phoc_def, doc, page, region), MapType(StringType(), ArrayType(IntegerType())))
