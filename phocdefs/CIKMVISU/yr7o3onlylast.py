from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x0y7r7o3_only_last() -> PhocCompose:

    to_compose = [
        HorzLinePhoc(0, 6, starting_level=6),
        RectanglePhoc(6, starting_level=6),
        EllipsisPhoc(2, starting_level=2)
    ]

    compose = PhocCompose(to_compose, skip_first_level=True)

    return compose
