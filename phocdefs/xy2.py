from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_xy2() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 1)
    horz_phoc = HorzLinePhoc(0, 1)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        ])
    return compose