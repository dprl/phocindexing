from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_example() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 4)
    horz_phoc = HorzLinePhoc(20, 3)
    circle_phoc = EllipsisPhoc(
        degree_n=4,
        glue_to_height=False,
        glue_to_width=True,
        #glue_to_larger=True,
        scale_factor=5
    )
    circle_phoc2 = EllipsisPhoc(
        degree_n=3,
        glue_to_height=False,
        glue_to_width=True,
        scale_factor=.4,
        rotate_degree=55
    )
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        circle_phoc,
        circle_phoc2
    ])

    return compose
