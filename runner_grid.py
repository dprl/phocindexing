from phocdefs.build_compose import build_compose
import sys
import os
from datetime import datetime

run_from = int(sys.argv[1])
run_to = int(sys.argv[2])

count_configs = 0
domain = [x for x in range(1, 12, 2)]
domain.insert(0, 0)
for x in domain:
    for y in domain:
        for r in domain:
            for o in domain:
                if build_compose(x, y, r, o).explain()[1] > 64 or (x == y and y == r and r == o and o == 0):
                    continue
                if run_from <= count_configs <= run_to:
                    now = datetime.now()
                    current_time = now.strftime("%H:%M:%S")

                    run_name = f"x{x}y{y}r{r}o{o}"
                    print(f"#{count_configs} --> {run_name} TIME: {current_time}")
                    command_run = f"./bin/runner configs/CIKM/{run_name}.ini > CIKM_Log/{run_name}_log.txt 2>&1"
                    # command_retrieval = f"./run-retrieval configs/CIKM/{run_name}.ini > CIKM_Log/{run_name}_log_retrieval.txt 2>&1"
                    command_results = f"cat ./outputs/CIKM/eval/{run_name}_results.tsv"
                    os.system(command_run)
                    os.system("curl -XDELETE -k -s -u \"admin:admin\" \"https://localhost:9200/x*\"")
                    # os.system(command_retrieval)
                    os.system(command_results)
                    print()
                count_configs += 1
