import math
from typing import Dict, List
from opensearchpy import OpenSearch


def get_mapping_keys(index_name, client: OpenSearch) -> List[str]:
    return list(client.indices.get_mapping()[index_name]['mappings']['properties'].keys())


def get_total_docs(index_name, client: OpenSearch) -> int:
    return int(client.cat.count(index=index_name).split()[2])


def get_token_freq(index_name, client: OpenSearch, token) -> int:
    if token == "*":
        token = "\\*"
    req = {
      "query": {
        "bool": {
          "must": [
            {"exists": {"field": token}}
          ]
        }
      }
    }
    return client.count(index=index_name, body=req)['count']


def get_idf_map(index_name, client: OpenSearch):
    token_idf_map = {}
    total_doc_count = get_total_docs(index_name, client)
    for token in get_mapping_keys(index_name, client):
        token_freq = get_token_freq(index_name, client, token)
        idf = math.log(total_doc_count/(token_freq+1))
        token_idf_map[token] = idf
    return token_idf_map


def phoc_to_binary_map(row):
    phoc: Dict = row["phoc"]
    for k in phoc.keys():
        phoc[k] = 1
    return phoc


def get_n_k_from_df(phoc1, phoc2):

    def add_to_dict(key, old_dict: Dict, d: Dict):
        if key in d.keys():
            cur = d[key]
            cur += old_dict[key]
            d[key] = cur
        else:
            d[key] = old_dict[k]

    reduced_dict = {}
    for k in phoc1.keys():
        add_to_dict(k, phoc1, reduced_dict)

    for k in phoc2.keys():
        add_to_dict(k, phoc2, reduced_dict)

    return reduced_dict


def get_idf_func(collection_size: int):
    def compute_idf(n_k_dict: Dict):
        idf_dict = {}
        for k in n_k_dict.keys():
            n_k = n_k_dict[k]
            denom = math.log(collection_size/(n_k+1))
            if denom == 0:
                idf_dict[k] = 0
            else:
                idf_dict[k] = denom
        return idf_dict
    return compute_idf


def get_pos_idf_from_df(row1, row2):
    pass


def filter_idf_map_for_bulk(idf_dict: Dict, phoc: Dict):
    filtered_dict = {}
    for token in phoc:
        if token != "b_norm_factor" and token != "post_id":
            filtered_dict[token + '_idf'] = idf_dict[token]
    return filtered_dict


