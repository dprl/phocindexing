import csv
import glob
import json
import os
import sys
from io import StringIO
from typing import Set

from debugrender.count_rendered_ids import id_in_set

csv.field_size_limit(sys.maxsize)


def get_ids_from_file(f, s_global) -> int:
    s = set()
    count = 0
    first_in_pair = True
    with open(f, encoding="utf8", errors='ignore') as file:
        for line in file:
            current = json.load(StringIO(line))
            if first_in_pair:
                count += 1
                idx = int(current["index"]["_id"])
                first_in_pair = False
                if not id_in_set(s, idx, f, f):
                    s.add(idx)
                if not id_in_set(s_global, idx, f, "global"):
                    s_global.add(idx)
            else:
                first_in_pair = True

    return count


if __name__ == "__main__":
    global_set = set()
    total_count = 0
    path_to_rendered = os.path.join("outputs", "visu", "json_chunks", "xy5", "*.json")
    for cache_file in glob.glob(path_to_rendered):

        total_count += get_ids_from_file(cache_file, global_set)

    print(f"{total_count} total formulas")