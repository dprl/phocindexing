mask_idf = {
    "script": {
    "lang": "painless",
    "source": """
    double totalBitCount = 0;
    for (String tokenKey : params['tokens']) {
      long i = params[tokenKey];
      String idfToken = tokenKey+'_idf';

      java.util.ArrayList mask = params['mask'];

      if (doc.containsKey(tokenKey) && doc[tokenKey].size()>0) {
        Long longIntDoc = doc[tokenKey].get(0);
        Float idf = doc[idfToken].get(0).floatValue();
        long docInt = longIntDoc.longValue();
        long masked = docInt & i;
        
        int lastIndex = params['vectorLength']-1;
        
        String strIntArr = Long.toBinaryString(masked);
        int sum = 0;
        for (int c=strIntArr.length()-1; c>=0; c--){
            sum += Integer.parseInt(""+strIntArr.charAt(c)) * mask.get(lastIndex);
            lastIndex--;
        }
        totalBitCount += sum.floatValue() * idf.floatValue();
        
      }
    }
    double norm_factor = doc['b_norm_factor'].getValue().doubleValue();
    return (double) totalBitCount/(double) norm_factor;"""
  }
}