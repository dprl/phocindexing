import ctypes
import json
from typing import Dict, Any, Hashable, List
from opensearchpy import OpenSearch
from phocdefs import phoc_defs
from anyphoc.canvas.converters.svgcanvas import svg_to_svg_canvas, render
from anyphoc.indexutils.compressbitvectors import compress_bitvectors
from anyphoc.phoc.phoccompose import PhocCompose
from userdefinedfunctions.build_batch_insert import get_norm_factor


def build_json_query(
        svg: str,
        phoc_def: PhocCompose,
        vector_length: int,
        retrieval_func: str,
        percent_should: str,
        region_repr: str,
        cand_scale: float,
        expand_policy: str,
        mask: List[float],
        mask_op: str) -> Dict:
    index_type = ctypes.c_int64
    score_func = "phoc-long-score"
    
    if retrieval_func != "" and retrieval_func != "symbol-percentage":
        score_func = score_func + '-' + retrieval_func

    if mask is None:
        mask = [1]*vector_length
    if retrieval_func == "symbol-percentage":
        index_type = retrieval_func
        score_func = "phoc-symbol-percentage-score"

    canvas_ = svg_to_svg_canvas(svg, region_repr, cand_scale, expand_policy)

    tested = phoc_def.test(canvas_)
    
    compressed: Dict[str, Any] = compress_bitvectors(tested, index_type)
    tokens = list(compressed.keys())
    if '.' in tokens:
        tokens.remove('.')
    should_list = make_should_list(tokens)
    compressed['tokens'] = tokens
    compressed['mask'] = mask
    compressed['mask_op'] = mask_op

    compressed['norm_query'] = get_norm_factor(tested)
    compressed['vectorLength'] = len(mask)

    json_req_dict = {
        "size": 1000,
        "query": {
            "script_score": {
                "query": {
                    "bool": {
                        "should": should_list,
                        "minimum_should_match": percent_should
                    }
                },
                "script": {
                    "id": score_func,
                    "params": compressed
                }
            }
        }
    }

    return json_req_dict


def make_should_list(tokens) -> List:
    return [{"exists": {"field": t}} for t in tokens]


if __name__ == '__main__':
    host = 'localhost'
    port = 9200
    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': host, 'port': port}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        # client_cert = client_cert_path,
        # client_key = client_key_path,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    svg_data = render(r"n\times n", "http://127.0.0.1:8045/render/")
    compose = phoc_defs['XY5']()
    query = build_json_query(svg_data, compose, 64, retrieval_func="hex", percent_should="0%")
    with open('test.json', 'wb') as f:
        f.write(json.dumps(query, ensure_ascii=False, indent=2).encode('utf-8'))
    response = client.search(
        body=query,
        index='xy5small'
    )
    with open('testresp.json', 'wb') as f:
        f.write(json.dumps(response, ensure_ascii=False, indent=2).encode('utf-8'))
