import math
import os
from typing import List, Dict, Tuple

from tqdm.contrib.concurrent import process_map
from concurrent.futures import ProcessPoolExecutor
from load_index import get_data_from_file
from postprocess.post_process_utils import PostChunk, zip_post_chunks, elementwise_add, elementwise_mul, IdfChunk, \
    zip_idf_chunks
from process_utils import build_batch_insert


def get_pos_idf_rel_symbol_map(path_to_dir: str, workers, lower_bound: int) -> Dict[str, List[float]]:
    chunks = zip_idf_chunks(path_to_dir, 1)
    print(f"pos-idf map being extracted by {workers} workers")
    with ProcessPoolExecutor(max_workers=workers) as executor:
        chunk_results: List[Tuple] = list(executor.map(get_pos_idf_rel_symbol_map_task, chunks))
    # process_map(get_pos_idf_map_task, chunks, max_workers=workers, initializer=init_worker, initargs=(q,))
    symbol_pos_actual_sum: Dict[str, List[float]] = dict()
    symbol_pos_potential_sum: Dict[str, int] = dict()

    for chunk_result in chunk_results:
        chunk_potential_sum = chunk_result[1]
        for symbol in chunk_potential_sum:
            if symbol == 'b_norm_factor' \
                    or symbol == 'post_id' \
                    or symbol == '__unifiedkeys__':
                continue
            symbol_sum_from_chunk = symbol_pos_potential_sum.get(symbol)
            if symbol_sum_from_chunk is not None:
                symbol_pos_potential_sum[symbol] = symbol_pos_potential_sum[symbol] + chunk_potential_sum[symbol]
            else:
                symbol_pos_potential_sum[symbol] = chunk_potential_sum[symbol]

        chunk_actual_sum = chunk_result[0]
        for potential_new_symbol in chunk_actual_sum:
            if potential_new_symbol == 'b_norm_factor' \
                    or potential_new_symbol == 'post_id' \
                    or potential_new_symbol == '__unifiedkeys__':
                continue
            symbol_count_vector = symbol_pos_actual_sum.get(potential_new_symbol)

            if symbol_count_vector is not None:
                symbol_pos_actual_sum[potential_new_symbol] = \
                    elementwise_add(symbol_count_vector, chunk_actual_sum[potential_new_symbol])
            else:
                symbol_pos_actual_sum[potential_new_symbol] = chunk_actual_sum[potential_new_symbol]

    for symbol in symbol_pos_actual_sum:
        if symbol == 'b_norm_factor' \
                or symbol == 'post_id' \
                or symbol == '__unifiedkeys__':
            continue
        sum_total = symbol_pos_potential_sum[symbol]
        symbol_vect = symbol_pos_actual_sum[symbol]
        for pos in range(len(symbol_vect)):
            # where idf actually calculated
            sum_per_symbol: float = symbol_vect[pos]
            idf_term = max(math.log(sum_total / (sum_per_symbol + 1)), lower_bound)
            symbol_vect[pos] = idf_term

    return symbol_pos_actual_sum


def get_pos_idf_rel_symbol_map_task(idf_chunk: IdfChunk):
    pos_idf_dict: Dict[str, List[float]] = dict()
    potential_sum: Dict[str, int] = dict()

    data = get_data_from_file(idf_chunk.full_path_to_input_file)
    formulas = data[1:len(data):2]
    for formula in formulas:
        for symbol in formula:
            if symbol == 'b_norm_factor' or symbol == 'post_id' or symbol == '__unifiedkeys__':
                continue
            vect = formula[symbol]
            old_vect = pos_idf_dict.get(symbol)
            if old_vect is not None:
                pos_idf_dict[symbol] = elementwise_add(old_vect, vect)
            else:
                pos_idf_dict[symbol] = vect

            old_sum = potential_sum.get(symbol)
            if old_sum is not None:
                potential_sum[symbol] = potential_sum[symbol] + 1
            else:
                potential_sum[symbol] = 1

    return pos_idf_dict, potential_sum

