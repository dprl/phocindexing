import logging
from typing import Callable
from pyspark.sql.types import StringType, MapType, ArrayType, IntegerType
from pyspark.sql.functions import udf
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.canvas.converters.svgcanvas import svg_to_svg_canvas


def test_inner(x, idx, phoc_def: Callable[[], PhocCompose], region_type: str):
    try:        
        canvas_ = svg_to_svg_canvas(x, region_type)
        if canvas_ is None:
            return None
        return phoc_def().test(canvas_)
    except Exception as e:
        logger = logging.getLogger("broadcast")
        logger.warning(f"testing failed against row: {idx} caused by exception {e}, with svg: {x}")
        return None


def test(phoc_def: Callable[[None], PhocCompose], region_type: str) -> udf:
    return udf(lambda x, idx :test_inner(x, idx, phoc_def, region_type), MapType(StringType(), ArrayType(IntegerType())))
