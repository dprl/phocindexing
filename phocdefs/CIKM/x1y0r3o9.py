
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x1y0r3o9() -> PhocCompose:
    x=1
    y=0
    r=3
    o=9    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 0))
    if y>1:
        to_compose.append(HorzLinePhoc(0, -1))
    if r>1:
        to_compose.append(RectanglePhoc(2))
    if o>1:
        to_compose.append(EllipsisPhoc(8))
    compose = PhocCompose(to_compose)

    return compose
                