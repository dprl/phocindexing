from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
import math


def get_xy5_lin() -> PhocCompose:
    def f(x): return x
    vert_phoc = VertLinePhoc(0, 4, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 4, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ])
    return compose


def get_xy5_exp() -> PhocCompose:
    def f(x): return x**2
    vert_phoc = VertLinePhoc(0, 4, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 4, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ])
    return compose


def get_xy5_log() -> PhocCompose:
    def f(x): return math.log(x)
    vert_phoc = VertLinePhoc(0, 4, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 4, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ])
    return compose


def get_xy7o4_lin() -> PhocCompose:
    def f(x): return x
    vert_phoc = VertLinePhoc(0, 6, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 6, level_scale=f)
    ell_phoc = EllipsisPhoc(3, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ell_phoc
        ])
    return compose


def get_xy7o4_exp() -> PhocCompose:
    def f(x): return x**2
    vert_phoc = VertLinePhoc(0, 6, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 6, level_scale=f)
    ell_phoc = EllipsisPhoc(3, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ell_phoc
    ])
    return compose


def get_xy7o4_log() -> PhocCompose:
    def f(x): return math.log(x)
    vert_phoc = VertLinePhoc(0, 6, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 6, level_scale=f)
    ell_phoc = EllipsisPhoc(3, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ell_phoc
    ])
    return compose

# ========================================================================


def get_r9_lin() -> PhocCompose:
    def f(x): return x

    rect_phoc = RectanglePhoc(9, level_scale=f)
    compose = PhocCompose([
        rect_phoc
    ])
    return compose


def get_r9_exp() -> PhocCompose:
    def f(x): return x**2

    rect_phoc = RectanglePhoc(9, level_scale=f)
    compose = PhocCompose([
        rect_phoc
    ])
    return compose


def get_r9_log() -> PhocCompose:
    def f(x): return math.log(x)
    rect_phoc = RectanglePhoc(9, level_scale=f)
    compose = PhocCompose([
        rect_phoc
        ])
    return compose
