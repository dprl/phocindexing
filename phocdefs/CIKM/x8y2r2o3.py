
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x8y2r2o3() -> PhocCompose:
    x=8
    y=2
    r=2
    o=3    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 7))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 1))
    if r>1:
        to_compose.append(RectanglePhoc(1))
    if o>1:
        to_compose.append(EllipsisPhoc(2))
    compose = PhocCompose(to_compose)

    return compose
                