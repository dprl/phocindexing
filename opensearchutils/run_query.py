from opensearchpy import OpenSearch, helpers
from typing import List, Dict, Callable


def run_query_scan(client: OpenSearch, index_name: str, query: dict, chunk_size=1000, num_hits=1000) -> List[Dict]:
    # returns a list of responses that are in the form of dicts
    rows = []
    if chunk_size > num_hits:
        chunk_size = num_hits
    i = 0
    try:
        for response in helpers.scan(client=client,
                                     query=query,
                                     index=index_name,
                                     timeout="1000m",
                                     request_timeout=100_000,
                                     size=chunk_size,
                                     raise_on_error=False,
                                     preserve_order=True,
                                     scroll="30m"
                                     ):
            rows.append(response)
            i += 1

            if i > num_hits:
                break
    except Exception as e:
        print(e)

    return rows


def run_query(client: OpenSearch, index_name: str, query: dict, chunk_size=1000, num_hits=1000) -> List[Dict]:
    response = client.search(
        body=query,
        request_timeout=100_000,
        index=index_name,
        size=num_hits)
    return response['hits']['hits']


def run_query_acl(client: OpenSearch, index_names: [str], query: dict, chunk_size=1000, num_hits=1000) -> List[Dict]:
    response = client.msearch(
        body='{}\n'+str(query).replace("'", '"'),

        request_timeout=100_000,
        index=index_names,

    )
    hits = []
    for i in response['responses']:
        hits += i["hits"]["hits"]
    return hits


def run_query_with_backoff(
        f: Callable, client: OpenSearch, index_name: str, query: dict, chunk_size=1000, num_hits=1000) -> List[Dict]:
    rows = []
    query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "100%"
    while len(rows) < num_hits and query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] != "-1%":
        rows = f(client, index_name, query, chunk_size, num_hits)
        if query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "100%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "95%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "95%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "85%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "85%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "75%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "75%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "50%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "50%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "25%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "25%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "0%"
        elif query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] == "0%":
            query["query"]["script_score"]["query"]["bool"]["minimum_should_match"] = "-1%"
