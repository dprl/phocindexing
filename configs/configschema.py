from configparser import ConfigParser

from schema import Schema
from phocdefs import phoc_defs

config_schema = Schema({
    "meta": {
        "EXP_NAMESPACE": str,
        "EXP_NAME": str,
    },
    "svg": {
        "RENDER_PORT": str,
        "RENDER_URL": str,
        "MATHJAX_REPLICATION_FACTOR": str
    },
    "spark": {
        "SOURCE": lambda s: s in ["TSV", "SVG"],
        "SVG_PATH": str,
        "TSV_PATH": str,
        "MAPPING_FILE": str
    },
    "phoc": {
        "PHOC_DEF": lambda pd: pd in phoc_defs.keys(),
        "REPR": lambda r: r in ["line", "centroid", "bbox"],
        "RETRIEVAL_REPR": lambda r: r in ["line", "centroid", "bbox"],
        "SYMBOL_REGION_SCALE": str,
        "RETRIEVAL_SYMBOL_REGION_SCALE": str,
        "EXPAND_POLICY": lambda ep: ep in ["normal", "left_associate", "left_associate_with_parts", "full_with_parts"],
        "MASK_OP": lambda ep: ep in ["add", "mul"]

    },
    "post": {
        "SKIP_TO_POST": lambda post: post in ["true", "false"],
        "POST_PROCESS": lambda post: post in ["true", "false"],
        "IDF": lambda idf: idf in ["idf", "idfrelu", "posidf", "posidfrelu", "density", ""],
        "IDF_MAP_LOC": str,
    },
    "opensearch": {
        "HOST": str,
        "PORT": str,
        "INDEX_NAME": str,
        "RETRIEVAL_FUNC": lambda rf: rf in ["mask-idf", "tf", "mask", "idf", "", "symbol-percentage"],
        "CONJ_PERCENT": str
    },
    "metrics": {
        "QREL_FILE": str,
        "RESULTS_FILE": str,
        "RESULTS_FOLDER": str,
        "DEDUPLICATED_RESULTS": str,
        "PRE_POST_JSON_CHUNKS": str,
        "JSON_CHUNKS_DIR": str,
        "TOPIC_FILE": str,
        "EVALUATION_RESULTS": str
    }
})


def validate_config_with_schema(config: ConfigParser):
    the_dict = {}
    for section in config.sections():
        the_dict[section] = {}
        for key, val in config.items(section):
            the_dict[section][key.upper()] = val
    config_schema.validate(the_dict)
