long = {
    "script": {
        "lang": "painless",
        "source":
            """ int totalBitCount = 0;
                for (String tokenKey : params['tokens']) {
                    long i = params[tokenKey];
                    if (doc.containsKey(tokenKey) && doc[tokenKey].size()>0) {
                        Long longIntDoc = doc[tokenKey].get(0);
                        long docInt = longIntDoc.longValue();
                        long masked = docInt & i;
                        totalBitCount += Long.bitCount(masked);
                    }
                }
                double norm_factor = doc['b_norm_factor'].getValue().doubleValue();
                return (double) totalBitCount/norm_factor;"""
    }
}
