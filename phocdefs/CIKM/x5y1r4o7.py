
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x5y1r4o7() -> PhocCompose:
    x=5
    y=1
    r=4
    o=7    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 4))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 0))
    if r>1:
        to_compose.append(RectanglePhoc(3))
    if o>1:
        to_compose.append(EllipsisPhoc(6))
    compose = PhocCompose(to_compose)

    return compose
                