from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_o7() -> PhocCompose:

    ellip_phoc = EllipsisPhoc(6)
    compose = PhocCompose([
        ellip_phoc
        ])
    return compose
