x0y0r3o3
The Docker Engine you're using is running in swarm mode.

Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

To deploy your application across the swarm, use `docker stack deploy`.

phocindexing_opensearch-dashboards_1 is up-to-date
phocindexing_opensearch-node1_1 is up-to-date
phocindexing_opensearch-node2_1 is up-to-date
c367b62fd1dd8d1980fc29a539b0a0a5984d2418bf8ce722c809ee8b66014bce
docker: Error response from daemon: driver failed programming external connectivity on endpoint vigilant_brattain (be193a28cf2d98dd43cd099c475fc6280af9536551fff41c8dd2d53945d21c8a): Error starting userland proxy: listen tcp4 0.0.0.0:8045: bind: address already in use.

waiting for mathjax to start

waiting for opensearch to start
index 'x0y0r3o3' does not yet exist. It will be created.

Registering scoring functions
phoc-long-score {'acknowledged': True}
phoc-long-score-idf {'acknowledged': True}
phoc-symbol-percentage-score {'acknowledged': True}
phoc-long-score-mask {'acknowledged': True}

Generating opensearch bulk insert file and mapping file, if it doesnt already exist

Rendered svgs have been split into 32 chunks and are being tested against by 10 workers
  0%|          | 0/32 [00:00<?, ?it/s]  3%|▎         | 1/32 [01:07<34:45, 67.27s/it] 12%|█▎        | 4/32 [01:11<06:24, 13.74s/it] 22%|██▏       | 7/32 [01:17<03:07,  7.50s/it] 34%|███▍      | 11/32 [01:50<02:45,  7.86s/it] 38%|███▊      | 12/32 [01:55<02:30,  7.54s/it] 41%|████      | 13/32 [02:06<02:32,  8.02s/it] 50%|█████     | 16/32 [02:10<01:21,  5.10s/it] 62%|██████▎   | 20/32 [02:29<00:59,  4.99s/it] 66%|██████▌   | 21/32 [02:50<01:18,  7.16s/it] 72%|███████▏  | 23/32 [03:02<01:02,  6.94s/it] 81%|████████▏ | 26/32 [03:04<00:26,  4.46s/it] 88%|████████▊ | 28/32 [03:07<00:15,  3.76s/it] 94%|█████████▍| 30/32 [03:08<00:05,  2.80s/it] 97%|█████████▋| 31/32 [03:20<00:04,  4.30s/it]100%|██████████| 32/32 [03:30<00:00,  5.52s/it]100%|██████████| 32/32 [03:30<00:00,  6.59s/it]
loading index from ./outputs/CIKM/json_chunks/x0y0r3o3/*.json
  0%|          | 0/32 [00:00<?, ?it/s] 28%|██▊       | 9/32 [00:00<00:00, 84.50it/s] 56%|█████▋    | 18/32 [00:05<00:05,  2.59it/s] 94%|█████████▍| 30/32 [00:06<00:00,  5.36it/s]100%|██████████| 32/32 [00:08<00:00,  3.92it/s]
0 total failures
time taken to load bulk index files into OpenSearch in seconds:  8.17390513420105
total hits for qid 1 : 1000
query time in seconds: 0.025011539459228516
total hits for qid 8 : 1000
query time in seconds: 0.017744064331054688
total hits for qid 12 : 1000
query time in seconds: 0.01728224754333496
total hits for qid 26 : 1000
query time in seconds: 0.0153656005859375
total hits for qid 30 : 1000
query time in seconds: 0.016449928283691406
total hits for qid 33 : 1000
query time in seconds: 0.01909780502319336
total hits for qid 38 : 1000
query time in seconds: 0.020136356353759766
total hits for qid 47 : 1000
query time in seconds: 0.019979238510131836
total hits for qid 49 : 1000
query time in seconds: 0.019945144653320312
total hits for qid 52 : 1000
query time in seconds: 0.01872110366821289
total hits for qid 58 : 1000
query time in seconds: 0.020811080932617188
total hits for qid 70 : 1000
query time in seconds: 0.022495031356811523
total hits for qid 73 : 1000
query time in seconds: 0.022986888885498047
total hits for qid 75 : 1000
query time in seconds: 0.020412921905517578
total hits for qid 90 : 1000
query time in seconds: 0.018069028854370117
total hits for qid 104 : 1000
query time in seconds: 0.021990299224853516
total hits for qid 130 : 1000
query time in seconds: 0.020150423049926758
total hits for qid 135 : 1000
query time in seconds: 0.02043771743774414
total hits for qid 139 : 1000
query time in seconds: 0.02059626579284668
total hits for qid 140 : 1000
query time in seconds: 0.045970916748046875
total hits for qid 164 : 1000
query time in seconds: 0.0218813419342041
total hits for qid 174 : 1000
query time in seconds: 0.02050495147705078
total hits for qid 176 : 1000
query time in seconds: 0.022292613983154297
total hits for qid 213 : 1000
query time in seconds: 0.01999354362487793
total hits for qid 221 : 1000
query time in seconds: 0.01945781707763672
total hits for qid 224 : 1000
query time in seconds: 0.0175783634185791
total hits for qid 236 : 1000
query time in seconds: 0.020084857940673828
total hits for qid 251 : 1000
query time in seconds: 0.022513389587402344
total hits for qid 252 : 1000
query time in seconds: 0.021741390228271484
total hits for qid 259 : 1000
query time in seconds: 0.020218849182128906
total hits for qid 267 : 1000
query time in seconds: 0.01675271987915039
total hits for qid 271 : 1000
query time in seconds: 0.019822120666503906
total hits for qid 281 : 1000
query time in seconds: 0.018013954162597656
total hits for qid 299 : 1000
query time in seconds: 0.02184772491455078
total hits for qid 302 : 1000
query time in seconds: 0.02058553695678711
total hits for qid 311 : 1000
query time in seconds: 0.022750377655029297
total hits for qid 335 : 1000
query time in seconds: 0.016716718673706055
total hits for qid 352 : 1000
query time in seconds: 0.01615428924560547
total hits for qid 369 : 1000
query time in seconds: 0.01909041404724121
total hits for qid 374 : 1000
query time in seconds: 0.044241905212402344
total hits for qid 376 : 1000
query time in seconds: 0.01973271369934082
total hits for qid 388 : 1000
query time in seconds: 0.025099992752075195
total hits for qid 393 : 1000
query time in seconds: 0.01994633674621582
total hits for qid 407 : 1000
query time in seconds: 0.017028331756591797
total hits for qid 411 : 1000
query time in seconds: 0.027035951614379883
total hits for qid 424 : 1000
query time in seconds: 0.022417068481445312
total hits for qid 436 : 1000
query time in seconds: 0.020694494247436523
total hits for qid 443 : 1000
query time in seconds: 0.021534442901611328
total hits for qid 466 : 1000
query time in seconds: 0.01899266242980957
total hits for qid 470 : 1000
query time in seconds: 0.023179054260253906
total hits for qid 481 : 1000
query time in seconds: 0.020017385482788086
total hits for qid 484 : 1000
query time in seconds: 0.022601842880249023
total hits for qid 496 : 1000
query time in seconds: 0.02602839469909668
total hits for qid 497 : 1000
query time in seconds: 0.02388906478881836
total hits for qid 500 : 1000
query time in seconds: 0.02579021453857422
total hits for qid 508 : 1000
query time in seconds: 0.023000240325927734
total hits for qid 511 : 1000
query time in seconds: 0.02433013916015625
total hits for qid 513 : 1000
query time in seconds: 0.02306079864501953
total hits for qid 530 : 1000
query time in seconds: 0.023389101028442383
total hits for qid 534 : 1000
query time in seconds: 0.025386571884155273
total hits for qid 540 : 1000
query time in seconds: 0.025086641311645508
total hits for qid 555 : 1000
query time in seconds: 0.024323463439941406
total hits for qid 558 : 1000
query time in seconds: 0.020810842514038086
total hits for qid 561 : 1000
query time in seconds: 0.020998716354370117
total hits for qid 562 : 1000
query time in seconds: 0.02098226547241211
total hits for qid 581 : 1000
query time in seconds: 0.020166873931884766
total hits for qid 589 : 1000
query time in seconds: 0.025194406509399414
total hits for qid 591 : 1000
query time in seconds: 0.02030348777770996
total hits for qid 598 : 1000
query time in seconds: 0.024544715881347656
total hits for qid 600 : 1000
query time in seconds: 0.02400970458984375
total hits for qid 607 : 1000
query time in seconds: 0.024846553802490234
total hits for qid 626 : 1000
query time in seconds: 0.022208452224731445
total hits for qid 630 : 1000
query time in seconds: 0.027249813079833984
total hits for qid 640 : 1000
query time in seconds: 0.028124094009399414
total hits for qid 647 : 1000
query time in seconds: 0.02497100830078125
total hits for qid 650 : 1000
query time in seconds: 0.023929119110107422
total hits for qid 656 : 1000
query time in seconds: 0.021485328674316406
total hits for qid 662 : 1000
query time in seconds: 0.020462751388549805
total hits for qid 671 : 1000
query time in seconds: 0.02625584602355957
total hits for qid 680 : 1000
query time in seconds: 0.030671358108520508
total hits for qid 689 : 1000
query time in seconds: 0.021480321884155273
total hits for qid 704 : 1000
query time in seconds: 0.023719310760498047
total hits for qid 709 : 1000
query time in seconds: 0.023166894912719727
total hits for qid 729 : 1000
query time in seconds: 0.02194356918334961
total hits for qid 732 : 1000
query time in seconds: 0.021700382232666016
total hits for qid 738 : 1000
query time in seconds: 0.023587942123413086
total hits for qid 740 : 1000
query time in seconds: 0.023810386657714844
total hits for qid 745 : 1000
query time in seconds: 0.025646209716796875
total hits for qid 766 : 1000
query time in seconds: 0.021936893463134766
total hits for qid 772 : 1000
query time in seconds: 0.02353382110595703
total hits for qid 780 : 1000
query time in seconds: 0.02765679359436035
total hits for qid 794 : 1000
query time in seconds: 0.024125337600708008
total hits for qid 795 : 1000
query time in seconds: 0.023537874221801758
total hits for qid 804 : 1000
query time in seconds: 0.023715496063232422
total hits for qid 808 : 1000
query time in seconds: 0.0248258113861084
total hits for qid 811 : 1000
query time in seconds: 0.026017427444458008
total hits for qid 814 : 1000
query time in seconds: 0.023908376693725586
total hits for qid 816 : 1000
query time in seconds: 0.02375006675720215
total hits for qid 834 : 1000
query time in seconds: 0.02522730827331543
total hits for qid 837 : 1000
query time in seconds: 0.027157306671142578
avg time req 0.02256124258041382
initial retrieval results: ./outputs/CIKM/initial_results/x0y0r3o3/x0y0r3o3_results.tsv
to view evaluation metrics see file: ./outputs/CIKM/eval/x0y0r3o3_results.tsv
./outputs/CIKM/eval/x0y0r3o3_results.tsv
