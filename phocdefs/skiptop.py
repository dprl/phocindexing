from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
import math


def get_xy7o4_no_top() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 6)
    horz_phoc = HorzLinePhoc(0, 6)
    ellip_phoc = EllipsisPhoc(3)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc

    ], skip_first_level=True)
    return compose


def get_xy7o4_skip2() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 6, starting_level=2)
    horz_phoc = HorzLinePhoc(0, 6, starting_level=2)
    ellip_phoc = EllipsisPhoc(3, starting_level=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
    ], skip_first_level=True)
    return compose


def get_xy5_no_top() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 4)
    horz_phoc = HorzLinePhoc(0, 4)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ], skip_first_level=True)
    return compose


def get_xy5_skip2() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 4, starting_level=2)
    horz_phoc = HorzLinePhoc(0, 4, starting_level=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ], skip_first_level=True)
    return compose


def get_xy5_odd() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 4, starting_level=2, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 4, starting_level=2, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
    ], skip_first_level=False)
    return compose


def get_xy5_even() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 4, starting_level=1, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 4, starting_level=1, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
    ], skip_first_level=True)
    return compose


def get_xy7o4_odd() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 6, starting_level=2, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 6, starting_level=2, skipping_step=2)
    ellip_phoc = EllipsisPhoc(3, starting_level=2, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
    ], skip_first_level=False)
    return compose


def get_xy7o4_even() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 6, starting_level=1, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 6, starting_level=1, skipping_step=2)
    ellip_phoc = EllipsisPhoc(3, starting_level=1, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
    ], skip_first_level=True)
    return compose


def get_xy10_odd() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 9, starting_level=2, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 9, starting_level=2, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
    ], skip_first_level=False)
    return compose


def get_xy10_even() -> PhocCompose:
    vert_phoc = VertLinePhoc(0, 9, starting_level=1, skipping_step=2)
    horz_phoc = HorzLinePhoc(0, 9, starting_level=1, skipping_step=2)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
    ], skip_first_level=True)
    return compose

