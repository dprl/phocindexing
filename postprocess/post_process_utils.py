import dataclasses
import glob
import json
import os
from typing import List, Dict


def build_mapping_file(destination_path, idf_symbol_dict, dim):
    to_write = {"properties": {}}
    for symbol in idf_symbol_dict:
        to_write["properties"][symbol] = {"type": "knn_vector", "dimension": dim, "index": False}

    with open(destination_path, 'w+') as mapping_file:
        mapping_file.write(json.dumps(to_write, ensure_ascii=False))


@dataclasses.dataclass
class IdfChunk:
    full_path_to_input_file: str
    lower_bound: int


def zip_idf_chunks(input_chunks_dir, lower_bound) -> List:
    input_files = glob.glob(input_chunks_dir, recursive=True)
    return [
        IdfChunk(
            in_file,
            lower_bound
        ) for in_file in input_files
    ]


@dataclasses.dataclass
class PostChunk:
    full_path_to_input_file: str
    full_path_to_output_dir: str
    map: Dict
    index_name: str


def zip_post_chunks(input_chunks_dir, output_chunks_dir, idf_map, index_name) -> List:
    input_files = glob.glob(input_chunks_dir, recursive=True)

    return [
        PostChunk(
            i,
            output_chunks_dir,
            idf_map,
            index_name
        ) for i in input_files
    ]


@dataclasses.dataclass
class DensityChunk:
    full_path_to_input_file: str
    full_path_to_output_dir: str
    densityMask: List
    index_name: str


def zip_density_chunks(input_chunks_dir, output_chunks_dir, density_mask, index_name) -> List:
    input_files = glob.glob(input_chunks_dir, recursive=True)

    return [
        DensityChunk(
            i,
            output_chunks_dir,
            density_mask,
            index_name
        ) for i in input_files
    ]


def elementwise_add(l1: List, l2: List) -> List:
    new_l = [0] * len(l1)
    for i in range(len(l1)):
        new_l[i] = l1[i] + l2[i]
    return new_l


def elementwise_mul(l1: List, l2: List) -> List:
    new_l = [0] * len(l1)
    for i in range(len(l1)):
        new_l[i] = l1[i] * l2[i]
    return new_l


def save_idf_map(idf_map_file, idf_dict):
    os.makedirs(os.path.split(idf_map_file)[0], exist_ok=True)
    with open(idf_map_file, 'w+') as idf_file:
        idf_file.write(json.dumps(idf_dict, ensure_ascii=False))
