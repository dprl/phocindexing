from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x7y5() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 4)
    horz_phoc = HorzLinePhoc(0, 6)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ])
    return compose