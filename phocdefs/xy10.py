from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_xy10() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 9)
    horz_phoc = HorzLinePhoc(0, 9)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ])
    return compose