import math
import os
from typing import Dict
from tqdm.contrib.concurrent import process_map
from load_index import get_data_from_file
from postprocess.post_process_utils import zip_idf_chunks, IdfChunk, zip_post_chunks, PostChunk
from concurrent.futures import ProcessPoolExecutor
from process_utils import build_batch_insert


def get_idf_map(input_chunks_dir: str, workers: int, lower_bound: int) -> Dict[str, float]:
    chunks = zip_idf_chunks(input_chunks_dir, 1)
    print(f"idf map being extracted by {workers} workers")
    with ProcessPoolExecutor(max_workers=workers) as executor:
        chunk_results = executor.map(get_idf_map_task, chunks, chunksize=1)

    # process_map(, max_workers=workers, initializer=init_worker, initargs=(q,))

    idf_dict: Dict[str, float] = dict()
    N = 0
    print("idf map being computed")
    for chunk_result in chunk_results:
        N += chunk_result[1]
        chunk_idf_dict = chunk_result[0]
        for potential_new_symbol in chunk_idf_dict:
            if potential_new_symbol == 'b_norm_factor' \
                    or potential_new_symbol == 'post_id'\
                    or potential_new_symbol == '__unifiedkeys__':
                continue
            symbol_count = idf_dict.get(potential_new_symbol)
            if symbol_count is not None:
                idf_dict[potential_new_symbol] = symbol_count + chunk_idf_dict[potential_new_symbol]
            else:
                idf_dict[potential_new_symbol] = chunk_idf_dict[potential_new_symbol]
    print("N is ", N)
    for symbol in idf_dict:
        num_formulas_with_symbol = idf_dict[symbol]
        idf_term = max(math.log(N / (num_formulas_with_symbol + 1)), 9.34341778591797)
        idf_dict[symbol] = idf_term

    return idf_dict


def get_idf_map_task(idf_chunk: IdfChunk):
    idf_dict: Dict[str, float] = dict()
    # files = glob.glob(, recursive=True)
    N = 0
    data = get_data_from_file(idf_chunk.full_path_to_input_file)
    formulas = data[1:len(data):2]
    for formula in formulas:
        N += 1
        for symbol in formula:
            if symbol == 'b_norm_factor' or symbol == 'post_id' or symbol == '__unifiedkeys__':
                pass
            else:
                old_float = idf_dict.get(symbol)
                if old_float is not None:
                    idf_dict[symbol] = old_float + 1
                else:
                    idf_dict[symbol] = 1

    return idf_dict, N


def apply_idf_map(input_chunks_dir: str, output_chunks_dir: str, idf_map: Dict, workers: int, index_name: str):
    chunks = zip_post_chunks(input_chunks_dir, output_chunks_dir, idf_map, index_name)
    print(f"idf map being applied by {workers} workers")
    process_map(apply_idf_map_task, chunks, max_workers=workers)


def apply_idf_map_task(post_chunk: PostChunk):

    data = get_data_from_file(post_chunk.full_path_to_input_file)
    file_name = os.path.split(post_chunk.full_path_to_input_file)[1]
    idf_map = post_chunk.map
    with open(os.path.join(post_chunk.full_path_to_output_dir, file_name), 'w+', newline='') as batch_insert_file:
        meta = data[0:len(data):2]
        formulas = data[1:len(data):2]
        for d in zip(meta, formulas):
            index_data = d[0]
            formula_data = d[1]
            tested = formula_data
            for symbol in tested:
                if symbol == 'b_norm_factor' or symbol == 'post_id':
                    continue
                scalar_idf_term = idf_map[symbol]
                tested[symbol] = [i*scalar_idf_term for i in tested[symbol]]
            idx = index_data["index"]["_id"]
            post_id = formula_data["post_id"]
            index_name = post_chunk.index_name
            norm = formula_data["b_norm_factor"]
            retrieval_func = "symbol-percentage"
            batch_insert_str = build_batch_insert(tested, idx, post_id, index_name, retrieval_func, norm)
            batch_insert_file.write(batch_insert_str + "\n")
