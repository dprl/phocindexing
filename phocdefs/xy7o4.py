from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
import math


def get_xy7o4() -> PhocCompose:
    f = lambda x: x**2
    vert_phoc = VertLinePhoc(0, 6, level_scale=f)
    horz_phoc = HorzLinePhoc(0, 6, level_scale=f)
    ellip_phoc = EllipsisPhoc(3, level_scale=f)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
    ])
    return compose
