long_idf = {
    "script": {
        "lang": "painless",
        "source":
            """double totalBitCount = 0;
            for (String tokenKey : params['tokens']) {
                long i = params[tokenKey];
                String idfToken = tokenKey+'_idf';
                if (doc.containsKey(tokenKey) && doc[tokenKey].size()>0) {
                    Long longIntDoc = doc[tokenKey].get(0);
                    Float idf = doc[idfToken].get(0).floatValue();
                    long docInt = longIntDoc.longValue();
                    long masked = docInt & i;
                    totalBitCount += Long.bitCount(masked) * idf;
                }
            }
            double norm_factor = doc['b_norm_factor'].getValue().doubleValue();
            return (double) totalBitCount/(double) norm_factor;"""
    }
}
