x3y9r1o3
The Docker Engine you're using is running in swarm mode.

Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

To deploy your application across the swarm, use `docker stack deploy`.

phocindexing_opensearch-node1_1 is up-to-date
phocindexing_opensearch-node2_1 is up-to-date
phocindexing_opensearch-dashboards_1 is up-to-date
82377872409aad18b34404d11bd42ec5215978f21909e08e2882ecddabf6ec2b
docker: Error response from daemon: driver failed programming external connectivity on endpoint clever_borg (5ca7060f350d9796dffbeebbb62040388f101cdd52260edba231aecb881e3e57): Error starting userland proxy: listen tcp4 0.0.0.0:8045: bind: address already in use.

waiting for mathjax to start

waiting for opensearch to start
index 'x3y9r1o3' does not yet exist. It will be created.

Registering scoring functions
phoc-long-score {'acknowledged': True}
phoc-long-score-idf {'acknowledged': True}
phoc-symbol-percentage-score {'acknowledged': True}
phoc-long-score-mask {'acknowledged': True}

Generating opensearch bulk insert file and mapping file, if it doesnt already exist

Rendered svgs have been split into 32 chunks and are being tested against by 10 workers
  0%|          | 0/32 [00:00<?, ?it/s]  3%|▎         | 1/32 [01:13<38:12, 73.96s/it]  9%|▉         | 3/32 [01:17<09:57, 20.61s/it] 12%|█▎        | 4/32 [01:19<06:34, 14.10s/it] 19%|█▉        | 6/32 [01:23<03:29,  8.07s/it] 31%|███▏      | 10/32 [01:24<01:16,  3.46s/it] 34%|███▍      | 11/32 [02:16<04:09, 11.88s/it] 38%|███▊      | 12/32 [02:20<03:27, 10.36s/it] 41%|████      | 13/32 [02:44<04:12, 13.29s/it] 66%|██████▌   | 21/32 [03:08<01:04,  5.89s/it] 72%|███████▏  | 23/32 [03:39<01:11,  7.93s/it] 78%|███████▊  | 25/32 [03:47<00:48,  6.98s/it] 88%|████████▊ | 28/32 [03:53<00:21,  5.38s/it] 94%|█████████▍| 30/32 [03:55<00:08,  4.36s/it]100%|██████████| 32/32 [04:08<00:00,  4.93s/it]100%|██████████| 32/32 [04:08<00:00,  7.78s/it]
loading index from ./outputs/CIKM/json_chunks/x3y9r1o3/*.json
  0%|          | 0/32 [00:00<?, ?it/s] 31%|███▏      | 10/32 [00:00<00:00, 95.97it/s] 62%|██████▎   | 20/32 [00:06<00:04,  2.75it/s] 94%|█████████▍| 30/32 [00:06<00:00,  4.94it/s]100%|██████████| 32/32 [00:08<00:00,  3.87it/s]
0 total failures
time taken to load bulk index files into OpenSearch in seconds:  8.267766237258911
total hits for qid 1 : 0
query time in seconds: 0.016162872314453125
total hits for qid 8 : 0
query time in seconds: 0.0051839351654052734
total hits for qid 12 : 0
query time in seconds: 0.006819009780883789
total hits for qid 26 : 0
query time in seconds: 0.0053060054779052734
total hits for qid 30 : 0
query time in seconds: 0.006089210510253906
total hits for qid 33 : 0
query time in seconds: 0.0066967010498046875
total hits for qid 38 : 0
query time in seconds: 0.009182453155517578
total hits for qid 47 : 0
query time in seconds: 0.005780458450317383
total hits for qid 49 : 0
query time in seconds: 0.0051190853118896484
total hits for qid 52 : 0
query time in seconds: 0.005112409591674805
total hits for qid 58 : 0
query time in seconds: 0.005078554153442383
total hits for qid 70 : 0
query time in seconds: 0.00802302360534668
total hits for qid 73 : 0
query time in seconds: 0.006845951080322266
total hits for qid 75 : 0
query time in seconds: 0.00536656379699707
total hits for qid 90 : 0
query time in seconds: 0.0056552886962890625
total hits for qid 104 : 0
query time in seconds: 0.005764007568359375
total hits for qid 130 : 0
query time in seconds: 0.005838871002197266
total hits for qid 135 : 0
query time in seconds: 0.005619049072265625
total hits for qid 139 : 0
query time in seconds: 0.0052907466888427734
total hits for qid 140 : 0
query time in seconds: 0.005625724792480469
total hits for qid 164 : 0
query time in seconds: 0.005461692810058594
total hits for qid 174 : 0
query time in seconds: 0.006165981292724609
total hits for qid 176 : 0
query time in seconds: 0.0053708553314208984
total hits for qid 213 : 0
query time in seconds: 0.005629062652587891
total hits for qid 221 : 0
query time in seconds: 0.00598907470703125
total hits for qid 224 : 0
query time in seconds: 0.006132602691650391
total hits for qid 236 : 0
query time in seconds: 0.005681753158569336
total hits for qid 251 : 0
query time in seconds: 0.005566596984863281
total hits for qid 252 : 0
query time in seconds: 0.00583648681640625
total hits for qid 259 : 0
query time in seconds: 0.006952762603759766
total hits for qid 267 : 0
query time in seconds: 0.005509376525878906
total hits for qid 271 : 0
query time in seconds: 0.005947113037109375
total hits for qid 281 : 0
query time in seconds: 0.0061187744140625
total hits for qid 299 : 0
query time in seconds: 0.00533747673034668
total hits for qid 302 : 0
query time in seconds: 0.007743358612060547
total hits for qid 311 : 0
query time in seconds: 0.006579399108886719
total hits for qid 335 : 0
query time in seconds: 0.005383014678955078
total hits for qid 352 : 0
query time in seconds: 0.005392313003540039
total hits for qid 369 : 0
query time in seconds: 0.0055065155029296875
total hits for qid 374 : 0
query time in seconds: 0.005247592926025391
total hits for qid 376 : 0
query time in seconds: 0.006013154983520508
total hits for qid 388 : 0
query time in seconds: 0.005517005920410156
total hits for qid 393 : 0
query time in seconds: 0.005158185958862305
total hits for qid 407 : 0
query time in seconds: 0.0052967071533203125
total hits for qid 411 : 0
query time in seconds: 0.006919145584106445
total hits for qid 424 : 0
query time in seconds: 0.0054590702056884766
total hits for qid 436 : 0
query time in seconds: 0.005158662796020508
total hits for qid 443 : 0
query time in seconds: 0.006384134292602539
total hits for qid 466 : 0
query time in seconds: 0.005223751068115234
total hits for qid 470 : 0
query time in seconds: 0.0053253173828125
total hits for qid 481 : 0
query time in seconds: 0.0053975582122802734
total hits for qid 484 : 0
query time in seconds: 0.005879640579223633
total hits for qid 496 : 0
query time in seconds: 0.0055522918701171875
total hits for qid 497 : 0
query time in seconds: 0.010866165161132812
total hits for qid 500 : 0
query time in seconds: 0.0064198970794677734
total hits for qid 508 : 0
query time in seconds: 0.005518674850463867
total hits for qid 511 : 0
query time in seconds: 0.00713348388671875
total hits for qid 513 : 0
query time in seconds: 0.0056149959564208984
total hits for qid 530 : 0
query time in seconds: 0.0053844451904296875
total hits for qid 534 : 0
query time in seconds: 0.0059850215911865234
total hits for qid 540 : 0
query time in seconds: 0.005224466323852539
total hits for qid 555 : 0
query time in seconds: 0.005209684371948242
total hits for qid 558 : 0
query time in seconds: 0.005424022674560547
total hits for qid 561 : 0
query time in seconds: 0.005335092544555664
total hits for qid 562 : 0
query time in seconds: 0.00551915168762207
total hits for qid 581 : 0
query time in seconds: 0.0052340030670166016
total hits for qid 589 : 0
query time in seconds: 0.0054132938385009766
total hits for qid 591 : 0
query time in seconds: 0.00534367561340332
total hits for qid 598 : 0
query time in seconds: 0.00560760498046875
total hits for qid 600 : 0
query time in seconds: 0.005372524261474609
total hits for qid 607 : 0
query time in seconds: 0.005894899368286133
total hits for qid 626 : 0
query time in seconds: 0.006047964096069336
total hits for qid 630 : 0
query time in seconds: 0.005618095397949219
total hits for qid 640 : 0
query time in seconds: 0.005830287933349609
total hits for qid 647 : 0
query time in seconds: 0.0069732666015625
total hits for qid 650 : 0
query time in seconds: 0.0052373409271240234
total hits for qid 656 : 0
query time in seconds: 0.006804943084716797
total hits for qid 662 : 0
query time in seconds: 0.004974842071533203
total hits for qid 671 : 0
query time in seconds: 0.005650043487548828
total hits for qid 680 : 0
query time in seconds: 0.0059833526611328125
total hits for qid 689 : 0
query time in seconds: 0.006391286849975586
total hits for qid 704 : 0
query time in seconds: 0.006372928619384766
total hits for qid 709 : 0
query time in seconds: 0.0053234100341796875
total hits for qid 729 : 0
query time in seconds: 0.005301713943481445
total hits for qid 732 : 0
query time in seconds: 0.00571131706237793
total hits for qid 738 : 0
query time in seconds: 0.005289316177368164
total hits for qid 740 : 0
query time in seconds: 0.0061016082763671875
total hits for qid 745 : 0
query time in seconds: 0.0059359073638916016
total hits for qid 766 : 0
query time in seconds: 0.005390644073486328
total hits for qid 772 : 0
query time in seconds: 0.005559206008911133
total hits for qid 780 : 0
query time in seconds: 0.005452156066894531
total hits for qid 794 : 0
query time in seconds: 0.005678892135620117
total hits for qid 795 : 0
query time in seconds: 0.005536794662475586
total hits for qid 804 : 0
query time in seconds: 0.0052547454833984375
total hits for qid 808 : 0
query time in seconds: 0.005180835723876953
total hits for qid 811 : 0
query time in seconds: 0.00516510009765625
total hits for qid 814 : 0
query time in seconds: 0.005501985549926758
total hits for qid 816 : 0
query time in seconds: 0.00541234016418457
total hits for qid 834 : 0
query time in seconds: 0.0056972503662109375
total hits for qid 837 : 0
query time in seconds: 0.005822896957397461
avg time req 0.005930979251861573
initial retrieval results: ./outputs/CIKM/initial_results/x3y9r1o3/x3y9r1o3_results.tsv
to view evaluation metrics see file: ./outputs/CIKM/eval/x3y9r1o3_results.tsv
Traceback (most recent call last):
  File "/home/ma5339/Documents/Projects/phocindexing/de_duplicate_2021.py", line 128, in <module>
    main()
  File "/home/ma5339/Documents/Projects/phocindexing/de_duplicate_2021.py", line 124, in main
    deduplicated_prim_file(submission_dir, result_dir, file, topic_visual_lst, dic_formula_visual_ids)
  File "/home/ma5339/Documents/Projects/phocindexing/de_duplicate_2021.py", line 83, in deduplicated_prim_file
    deduplicated_result, run_id = deduplicate(dic_formula_visual_ids, submission_dir + file, topic_visual_lst)
  File "/home/ma5339/Documents/Projects/phocindexing/de_duplicate_2021.py", line 56, in deduplicate
    result_dict, run = read_raw_submission_file(submission_file_path)
  File "/home/ma5339/Documents/Projects/phocindexing/de_duplicate_2021.py", line 51, in read_raw_submission_file
    return result_dic, row[5]
UnboundLocalError: local variable 'row' referenced before assignment
./outputs/CIKM/eval/x3y9r1o3_results.tsv
