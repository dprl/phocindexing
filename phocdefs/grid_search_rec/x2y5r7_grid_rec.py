
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x2y5r7_grid_rec() -> PhocCompose:
    x=1
    y=4
    r=6
    vert_phoc = VertLinePhoc(0, 1)
    horz_phoc = HorzLinePhoc(0, 4)
    rec_phoc = RectanglePhoc(6)
    to_compose=[]
    if x>0:
        to_compose.append(vert_phoc)
    if y>0:
        to_compose.append(horz_phoc)
    if r>0:
        to_compose.append(rec_phoc)

    compose = PhocCompose(to_compose)

    return compose
            