
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x6y4r7o1() -> PhocCompose:
    x=6
    y=4
    r=7
    o=1    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 5))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 3))
    if r>1:
        to_compose.append(RectanglePhoc(6))
    if o>1:
        to_compose.append(EllipsisPhoc(0))
    compose = PhocCompose(to_compose)

    return compose
                