import argparse
import os
import time
from configparser import ConfigParser, ExtendedInterpolation
import bs4
import json
from opensearchpy import OpenSearch, helpers
import pandas as pd
from anyphoc.canvas.converters.svgcanvas import render
from opensearchutils.jsonquery import build_json_query
from opensearchutils.run_query import run_query_scan, run_query
from phocdefs import phoc_defs


def convert_os_results_to_trec_row(hit, query_id, i, exp):
    rank = i
    formula_id = hit["_id"]
    if formula_id == "id" or formula_id is None:
        print("what the!!!!", hit)
    post_id = hit['_source']["post_id"]
    score = hit['_score']
    row = [query_id, formula_id, post_id, rank, score, exp]
    return row


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="configs/xy5.ini")
    parsed_args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(parsed_args.config)
    topic_file = config.get('metrics', 'TOPIC_FILE')
    render_url = config.get('svg', 'RENDER_URL')
    index_name = config.get('opensearch', 'INDEX_NAME').lower()
    phoc_def_string = config.get('phoc', 'PHOC_DEF')
    phoc_repr = config.get('phoc', 'RETRIEVAL_REPR')
    cand_scale = float(config.get('phoc', "RETRIEVAL_SYMBOL_REGION_SCALE"))
    expand_policy = config.get('phoc', "EXPAND_POLICY")
    mask_op = config.get('phoc', "MASK_OP")

    retrieval_func = config.get('opensearch', 'RETRIEVAL_FUNC')
    host = config.get('opensearch', "HOST")
    port = config.get('opensearch', 'PORT')
    result_file = config.get('metrics', 'RESULTS_FILE')
    conj_percent = config.get('opensearch', 'CONJ_PERCENT')
    eval_file = config.get('metrics', 'EVALUATION_RESULTS')

    experiment_namespace = config.get('meta', 'EXP_NAMESPACE')
    experiment_name = config.get('meta', 'EXP_NAME')

    with open(topic_file) as f:
        soup = bs4.BeautifulSoup(f.read(), 'xml')

    topics = soup.find_all('Topic')
    final_df = pd.DataFrame(columns=['Query_Id', 'Formula_Id', 'Post_Id', 'Rank', 'Score', 'Run_Number'])
    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': host, 'port': port}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    exp = experiment_namespace+"_"+experiment_name

    avg_time = 0
    for topic in topics:
        latex = topic.find("Latex").text
        qid = int(topic.find("Formula_Id").text.strip('q_'))
        topic_id = topic['number']
        svg_data = render(latex, render_url, )

        compose = phoc_defs[phoc_def_string]()
        query = build_json_query(
            svg_data, compose, 64, retrieval_func, conj_percent, phoc_repr, cand_scale, expand_policy,
            compose.make_mask(), mask_op)
        if True: # str(qid) == "816":
            with open('816_test.json', 'wb') as f:
                f.write(json.dumps(query, ensure_ascii=False, indent=2).encode('utf-8'))
        starttime = time.time()
        thresh = 30_000
        rows = run_query(client, index_name, query, num_hits=1000)

        treced_rows = [convert_os_results_to_trec_row(r, topic_id, i, exp) for i, r in enumerate(rows)]
        df = pd.DataFrame(treced_rows,
                          columns=['Query_Id', 'Formula_Id', 'Post_Id', 'Rank', 'Score', 'Run_Number'])
        final_df = pd.concat([final_df, df])
        endtime = time.time()
        reqtime = endtime - starttime
        print("total hits for qid", qid, ":", len(rows))
        print("query time in seconds:", reqtime)
        avg_time += reqtime
    split = os.path.split(result_file)

    print("avg time req", avg_time / len(topics))
    if split[0] != '':
        os.makedirs(split[0], exist_ok=True)

    print("initial retrieval results:", result_file)
    print("to view evaluation metrics see file:", eval_file)
    final_df.to_csv(result_file, header=False, sep="\t", index=False)
