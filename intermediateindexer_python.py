import asyncio
import math
import multiprocessing
import os.path
import os
import argparse
from configparser import ConfigParser, ExtendedInterpolation

from tqdm import tqdm

from configs.configschema import config_schema, validate_config_with_schema
from phocdefs import phoc_defs
from postprocess.density_mask_postprocess import get_density_mask, apply_density_mask
from postprocess.pos_idf_rel_symbol_postprocess import get_pos_idf_rel_symbol_map
from process_utils import split_rendered_svg_file, combine_batch_insert_files, test_helper, zip_chunks
from postprocess.post_process_utils import build_mapping_file, save_idf_map
from postprocess.pos_idf_postprocess import get_pos_idf_map, apply_pos_idf_map
from postprocess.idf_post_process import get_idf_map, apply_idf_map
from render_utils import render_formula_and_cache
from tqdm.contrib.concurrent import process_map


def main(args):
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(args.config)

    validate_config_with_schema(config)

    experiment_namespace = config.get('meta', 'EXP_NAMESPACE')
    experiment_name = config.get('meta', 'EXP_NAME')

    preferred_source = config.get('spark', 'SOURCE')
    tsv_path = config.get('spark', 'TSV_PATH')
    svg_path = config.get('spark', 'SVG_PATH')
    mapping_file = config.get('spark', 'MAPPING_FILE')
    render_url = config.get('svg', 'RENDER_URL')
    index_name = config.get('opensearch', 'INDEX_NAME').lower()
    phoc_def_string = config.get('phoc', 'PHOC_DEF')
    cand_repr_string = config.get('phoc', 'REPR')
    cand_scale = float(config.get('phoc', "SYMBOL_REGION_SCALE"))
    expand_policy = config.get('phoc', "EXPAND_POLICY")
    retrieval_func = config.get('opensearch', 'RETRIEVAL_FUNC')

    skip_to_post = config.get('post', 'SKIP_TO_POST') == 'true'
    do_post = config.get('post', 'POST_PROCESS') == 'true'
    idf_type = config.get('post', 'IDF')
    idf_map_file = config.get('post', 'IDF_MAP_LOC')

    # use only 90 percent of the cpus on any given machine so as not to overwhelm machine
    workers = math.floor(multiprocessing.cpu_count() * .90)
    final_json_chunks = config.get('metrics', 'JSON_CHUNKS_DIR')
    pre_post_json_chunks = config.get('metrics', 'PRE_POST_JSON_CHUNKS')
    if pre_post_json_chunks != "":
        out_path_chunks = pre_post_json_chunks
    else:
        # if not doing IDF style post process then the
        # original vectors we get from testing the
        # PHOC will be what we load into OS
        out_path_chunks = final_json_chunks
    if not skip_to_post:
        get_phoc_comp = phoc_defs[phoc_def_string]
        # render svgs if they do not exist, split the rendered file into chunks, so it can be processed in parallel.
        svg_file_path = os.path.join(svg_path, 'cache.tsv')
        split_path = os.path.join(svg_path, "split_rendered")
        force_re_render = False
        re_render = not os.path.exists(split_path)
        if re_render:
            print("Could not find rendered SVG files. Re-rendering now")

        if preferred_source == 'TSV' or re_render or force_re_render:
            print(f"loading TSV input from {tsv_path}, rendering and saving to {svg_path}")
            chunk_size = 1000
            os.makedirs(svg_path, exist_ok=True)
            asyncio.run(render_formula_and_cache(tsv_path, render_url, svg_file_path))
            split_rendered_svg_file(svg_file_path, chunk_size, split_path)
            # possibly delete full rendered once we have chunks to save space?

        os.makedirs(out_path_chunks, exist_ok=True)
        chunks = zip_chunks(
            split_path=split_path,
            get_phoc_comp_f=get_phoc_comp,
            cand_repr_string=cand_repr_string,
            cand_scale=cand_scale,
            expand_policy=expand_policy,
            index_name=index_name,
            out_path=out_path_chunks,
            retrieval_func=retrieval_func)

        debug = False
        if not debug:
            print(
                f"Rendered svgs have been split into {len(chunks)} "
                f"chunks and are being tested against by {workers} workers")
            process_map(test_helper, chunks, max_workers=workers)
        else:
            print(f"WARNING: YOU ARE IN DEBUG MODE. {len(chunks)} chunks being processed by one worker")
            for chunk in tqdm(chunks):
                test_helper(chunk)
    if do_post:
        os.makedirs(final_json_chunks, exist_ok=True)
        chunks_glob_path = os.path.join(out_path_chunks, "*")
        lower_bound = int("relu" in idf_type)
        if 'posidf' in idf_type:
            # change to input_chunks_path and output chunks path

            # idf_dict =
            # get_pos_idf_rel_symbol_map(path_to_dir=chunks_glob_path, workers=workers, lower_bound=lower_bound)
            idf_dict = get_pos_idf_map(path_to_dir=chunks_glob_path, workers=workers,
                                       lower_bound=lower_bound)
            save_idf_map(idf_map_file, idf_dict)
            apply_pos_idf_map(
                input_chunks_dir=chunks_glob_path,
                output_chunks_dir=final_json_chunks,
                idf_map=idf_dict, workers=workers, index_name=index_name)

        elif 'idf' in idf_type:
            idf_dict = get_idf_map(input_chunks_dir=chunks_glob_path, workers=workers, lower_bound=lower_bound)
            save_idf_map(idf_map_file, idf_dict)
            apply_idf_map(
                input_chunks_dir=chunks_glob_path,
                output_chunks_dir=final_json_chunks,
                idf_map=idf_dict, workers=workers, index_name=index_name)

        elif 'density' in idf_type:
            density_mask = get_density_mask(path_to_dir=chunks_glob_path, workers=workers)
            save_idf_map(idf_map_file, density_mask)
            apply_density_mask(
                input_chunks_dir=chunks_glob_path,
                output_chunks_dir=final_json_chunks,
                density_mask=density_mask, workers=workers, index_name=index_name)
            # only doing this here to get the idf_map cus that is what we use to build the mapping file

            idf_dict = get_idf_map(input_chunks_dir=chunks_glob_path, workers=workers, lower_bound=lower_bound)
        else:
            idf_dict = get_idf_map(input_chunks_dir=chunks_glob_path, workers=workers, lower_bound=lower_bound)

        get_phoc_comp = phoc_defs[phoc_def_string]
        phoc_def = get_phoc_comp()
        dim = phoc_def.explain()[1]
        build_mapping_file(mapping_file, idf_dict, dim)

    out_file_full_batch_insert = os.path.join(
        "outputs", experiment_namespace, "batch_insert", f"{experiment_name}.json")
    # os.makedirs(os.path.split(out_file_full_batch_insert)[0], exist_ok=True)
    # combine_batch_insert_files(final_json_chunks, out_file_full_batch_insert)
    # print(f"batch insert file generated and saved to {out_file_full_batch_insert}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="configs/xy5.ini")
    parsed_args = parser.parse_args()

    main(parsed_args)
