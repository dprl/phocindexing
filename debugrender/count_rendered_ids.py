import csv
import glob
import os
import sys
from typing import Set
csv.field_size_limit(sys.maxsize)


def get_ids_from_file(f, s_global) -> int:
    s = set()
    count = 0
    with open(f) as tsv_f:
        reader = csv.reader(tsv_f, delimiter='\t',  quotechar='"')
        next(reader)
        for line in reader:
            count += 1
            idx = int(line[0])
            if not id_in_set(s, idx,  f, f):
                s.add(idx)
            if not id_in_set(s_global, idx, f, "global"):
                s_global.add(idx)

    return count


def id_in_set(s: Set, idx, f, f2):
    if idx in s:
        print(f"duplicate id {idx} in file {f} relative to {f2}")
        return True
    return False


if __name__ == "__main__":
    global_set = set()
    total_count = 0
    path_to_rendered = os.path.join("outputs", "caches", "visu", "split_rendered", "*.tsv")
    for cache_file in glob.glob(path_to_rendered):

        total_count += get_ids_from_file(cache_file, global_set)

    print(f"{total_count} total formulas")
