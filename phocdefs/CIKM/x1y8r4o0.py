
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x1y8r4o0() -> PhocCompose:
    x=1
    y=8
    r=4
    o=0    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 0))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 7))
    if r>1:
        to_compose.append(RectanglePhoc(3))
    if o>1:
        to_compose.append(EllipsisPhoc(-1))
    compose = PhocCompose(to_compose)

    return compose
                