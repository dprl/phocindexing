from tkinter import Canvas
from typing import List, Tuple
import os

from tqdm import tqdm

from anyphoc.canvas.converters.procanvas import protable_to_region_canvas_list
from anyphoc.protables.ProTables import read_pro_tsv


def load_canvases(file_dir, region_type) -> List[Tuple[str, int, int, Canvas]]:
    final_list = []

    all_files = os.listdir(file_dir)
    tsv_files = list(filter(lambda f: f.endswith('.tsv'), all_files))
    print("loading canvases")
    for file in tqdm(tsv_files):

        pro = read_pro_tsv(os.path.join(file_dir, file))
        l_of_l_of_canvases = protable_to_region_canvas_list(pro, region_type)
        for document in l_of_l_of_canvases:
            for canvas in document:
                if canvas is not None:
                    doc = canvas.file_name
                    page = canvas.page_number
                    region = canvas.formula_region
                    final_list.append((doc, page, region, canvas))

                else:
                    print("canvas was none!")

    return final_list
