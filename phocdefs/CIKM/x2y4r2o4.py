
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x2y4r2o4() -> PhocCompose:
    x=2
    y=4
    r=2
    o=4    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 1))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 3))
    if r>1:
        to_compose.append(RectanglePhoc(1))
    if o>1:
        to_compose.append(EllipsisPhoc(3))
    compose = PhocCompose(to_compose)

    return compose
                