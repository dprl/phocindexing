from opensearchutils.phoclongscoreidf import long_idf
from opensearchutils.phoclongscore import long
from opensearchutils.phocvectorscore import vector
from opensearchutils.phoclongscoremask import mask
from typing import Dict
from opensearchpy import OpenSearch


def load_scoring_functions():
    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': 'localhost', 'port': '9200'}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    print("phoc-long-score", client.put_script("phoc-long-score", body=long))
    print("phoc-long-score-idf", client.put_script("phoc-long-score-idf", body=long_idf))
    print("phoc-symbol-percentage-score", client.put_script("phoc-symbol-percentage-score", body=vector))
    print("phoc-long-score-mask", client.put_script("phoc-long-score-mask", body=mask))



if __name__ == "__main__":
    load_scoring_functions()
