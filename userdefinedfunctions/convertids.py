from pyspark.sql.functions import udf, col
from pyspark.sql.types import IntegerType


def convert_ids_inner(str_id):
    if type(str_id) == str:
        return int(str_id.strip('q_'))
    elif type(str_id) == int:
        return str_id
    else:
        Exception('Error unsupported type')


def convert_ids() -> udf:
    return udf(lambda x: convert_ids_inner(x), IntegerType())