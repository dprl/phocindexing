import os
from configparser import ConfigParser, ExtendedInterpolation
from subprocess import check_output
import argparse


def calculated_ndcg(res_directory, trec_eval_tool, qre_file_path):
    result = {}
    os.makedirs(res_directory, exist_ok=True)
    for file in os.listdir(res_directory):
        output = check_output([trec_eval_tool, qre_file_path, res_directory + file, "-m", "ndcg"])
        output = output.decode('utf-8')
        score = output.split("\t")[2].strip()
        submission = file.split(".")[0].split("prim_")[1]
        result[submission] = score
    return result


def calculated_map(res_directory, trec_eval_tool, qre_file_path):
    result = {}
    for file in os.listdir(res_directory):
        output = check_output([trec_eval_tool, qre_file_path, res_directory + file, "-l2", "-m", "map"])
        output = output.decode('utf-8')
        score = output.split("\t")[2].strip()
        submission = file.split(".")[0].split("prim_")[1]
        result[submission] = score
    return result


def calculated_p_at_10(res_directory, trec_eval_tool, qre_file_path):
    result = {}
    for file in os.listdir(res_directory):
        output = check_output([trec_eval_tool, qre_file_path, res_directory + file, "-l2", "-m", "P"])
        output = output.decode('utf-8').split("\n")[1]
        score = output.split("\t")[2].strip()
        submission = file.split(".")[0].split("prim_")[1]
        result[submission] = score
    return result


def calculated_p_at_5(res_directory, trec_eval_tool, qre_file_path):
    result = {}
    for file in os.listdir(res_directory):
        output = check_output([trec_eval_tool, qre_file_path, res_directory + file, "-l2", "-m", "P"])
        output = output.decode('utf-8').split("\n")[0]
        score = output.split("\t")[2].strip()
        submission = file.split(".")[0].split("prim_")[1]
        result[submission] = score
    return result


def calculated_p_at_1(res_directory, trec_eval_tool, qre_file_path):
    result = {}
    for file in os.listdir(res_directory):
        output = check_output([trec_eval_tool, qre_file_path, res_directory + file, "-l2", "-m", "P.1"])
        output = output.decode('utf-8')
        score = output.split("\t")[2].strip()
        submission = file.split(".")[0].split("prim_")[1]
        result[submission] = score
    return result


def get_result(trec_eval_tool, qre_file_path, prim_result_dir, evaluation_result_file):
    if os.path.split(evaluation_result_file)[0] != '':
        os.makedirs(os.path.split(evaluation_result_file)[0], exist_ok=True)
    file_res = open(evaluation_result_file, "w+")
    res_ndcg = calculated_ndcg(prim_result_dir, trec_eval_tool, qre_file_path)
    res_map = calculated_map(prim_result_dir, trec_eval_tool, qre_file_path)
    res_p10 = calculated_p_at_10(prim_result_dir, trec_eval_tool, qre_file_path)
    res_p5 = calculated_p_at_5(prim_result_dir, trec_eval_tool, qre_file_path)
    res_p1 = calculated_p_at_1(prim_result_dir, trec_eval_tool, qre_file_path)
    file_res.write("System\tnDCG'\tmAP'\tp@10\tp@5\tp@1\n")
    for sub in res_ndcg:
        file_res.write(
            str(sub) + "\t" +
            str(res_ndcg[sub]) + "\t" +
            str(res_map[sub]) + "\t" +
            str(res_p10[sub]) + "\t" +
            str(res_p5[sub]) + "\t" +
            str(res_p1[sub]) + "\n")
    file_res.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config")
    parsed_args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(parsed_args.config)

    trec_eval_tool = "./trec_eval/trec_eval"
    qre_file_path = config.get('metrics', 'QREL_FILE')
    prim_result_dir = config.get('metrics', 'DEDUPLICATED_RESULTS')
    evaluation_result_file = config.get('metrics', 'EVALUATION_RESULTS')

    print(evaluation_result_file)
    get_result(trec_eval_tool, qre_file_path, prim_result_dir, evaluation_result_file)


if __name__ == "__main__":
    main()
