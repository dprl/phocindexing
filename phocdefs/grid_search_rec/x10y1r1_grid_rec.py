
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x10y1r1_grid_rec() -> PhocCompose:
    x=9
    y=0
    r=0
    vert_phoc = VertLinePhoc(0, 9)
    horz_phoc = HorzLinePhoc(0, 0)
    rec_phoc = RectanglePhoc(0)
    to_compose=[]
    if x>0:
        to_compose.append(vert_phoc)
    if y>0:
        to_compose.append(horz_phoc)
    if r>0:
        to_compose.append(rec_phoc)

    compose = PhocCompose(to_compose)

    return compose
            