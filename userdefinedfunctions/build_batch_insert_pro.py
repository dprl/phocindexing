import ctypes
import json

from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from anyphoc.indexutils.compressbitvectors import compress_bitvectors
from userdefinedfunctions.build_batch_insert import get_norm_factor


def build_batch_insert_inner_pro(vectors, idx, doc: str, page: int, region: int, index_name, index_type) -> str:
    try:
        compressed = compress_bitvectors(vectors, index_type)
        compressed["b_norm_factor"] = get_norm_factor(vectors)
        compressed["doc"] = doc
        compressed["page"] = page
        compressed["region"] = region
        compressed_json = json.dumps(compressed, ensure_ascii=False)
        return """{{ "index" : {{ "_index": "{index_name}", "_id" : "{idx}" }} }}
{mapjson}""".format(idx=idx, mapjson=compressed_json, index_name=index_name)
    except Exception as e:
        print(f'2failed to build json id {idx} vectors {vectors}')
        return ""


def build_batch_insert_pro(index_name, vector_length) -> udf:
    if 0 < vector_length <= 8:
        index_type = ctypes.c_int8
    elif 8 < vector_length <= 16:
        index_type = ctypes.c_int16
    elif 16 < vector_length <= 32:
        index_type = ctypes.c_int32
    elif 32 < vector_length <= 64:
        index_type = ctypes.c_int64
    else:
        index_type = "hex"
    return udf(lambda vectors, idx, doc, page, region:
               build_batch_insert_inner_pro(vectors, idx, doc, page, region, index_name, index_type), StringType())