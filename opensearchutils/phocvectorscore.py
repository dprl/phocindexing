vector = {
    "script": {
        "lang": "painless",
        "source":
            """double totalCount = 0.0;
            for (String tokenKey: params['tokens']) { 
                if (doc.containsKey(tokenKey) && doc[tokenKey].size() > 0) { 
                    float[] tokenVect = doc[tokenKey].getValue();
                    ArrayList queryVect = params[tokenKey];
                    double acc = 0;
                    for (int i =0; i<tokenVect.length; i++) {
                        acc += tokenVect[i]*queryVect[i];
                    }
                    totalCount += acc;
                }
            }
            double norm_factor = doc['b_norm_factor'].getValue().doubleValue();
            return totalCount / norm_factor;"""
    }
}
