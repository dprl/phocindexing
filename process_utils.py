import csv
import glob
import json
import os
import time
import traceback
from dataclasses import dataclass
from typing import Callable
import sys
from anyphoc.canvas.converters.svgcanvas import svg_to_svg_canvas
from anyphoc.indexutils.compressbitvectors import compress_bitvectors
from anyphoc.phoc.phoccompose import PhocCompose
from opensearchutils.idf import filter_idf_map_for_bulk
from userdefinedfunctions.build_batch_insert import get_norm_factor
from load_index import get_data_from_file


def split_rendered_svg_file(svg_path, part_size: int, out_path):
    os.makedirs(out_path, exist_ok=True)
    command = f"split -l {part_size} -d --additional-suffix .tsv {svg_path} {os.path.join(out_path, 'split_')}"
    os.system(command)
    time.sleep(1)


def combine_batch_insert_files(batch_files_path, out_file):
    os.system(f"echo -n '' > {out_file}")
    files = glob.glob(os.path.join(batch_files_path, "*.json"))
    for file in files:
        os.system(f"cat {file} >> {out_file}")
        os.system(f"echo '' >> {out_file}")


@dataclass
class Chunk:
    cur_file: str
    get_phoc_comp_f: Callable[[], PhocCompose]
    cand_repr_string: str
    cand_scale: float
    expand_policy: str
    index_name: str
    out_path: str
    retrieval_func: str


def zip_chunks(
        split_path: str,
        get_phoc_comp_f: Callable[[], PhocCompose],
        cand_repr_string: str,
        cand_scale: float,
        expand_policy: str,
        index_name: str,
        out_path: str,
        retrieval_func: str):
    files = glob.glob(os.path.join(split_path, "*.tsv"))
    return [
        Chunk(
            file,
            get_phoc_comp_f,
            cand_repr_string,
            cand_scale,
            expand_policy,
            index_name,
            out_path,
            retrieval_func
        )
        for file in files
    ]


def test_helper(chunk: Chunk) -> None:
    csv.field_size_limit(sys.maxsize)
    compose = chunk.get_phoc_comp_f()
    file_name = os.path.split(chunk.cur_file)[1]
    file_name_no_ext = os.path.splitext(file_name)[0]
    with open(os.path.join(chunk.out_path, f"{file_name_no_ext}.json"), 'w+', newline='') as batch_insert_file:
        with open(chunk.cur_file, newline='', encoding='utf-8') as csv_in_file:
            csv_reader = csv.reader(csv_in_file, delimiter='\t',  quotechar='"')
            for row in csv_reader:
                try:
                    idx = row[0]
                    post_id = row[1]
                    vis_id = row[4]
                    rendered_svg = row[6]
                    tested = test_inner(
                        rendered_svg,
                        idx,
                        vis_id,
                        compose,
                        chunk.cand_repr_string,
                        chunk.cand_scale,
                        chunk.expand_policy
                    )
                    if tested is not None:
                        batch_insert_str = build_batch_insert(tested, idx, post_id, chunk.index_name, chunk.retrieval_func)
                        batch_insert_file.write(batch_insert_str + "\n")
                except Exception as e:
                    print("---------------------------------------------------")
                    print("---------------------------------------------------")
                    print(e)
                    print(row)
                    print("---------------------------------------------------")
                    print("---------------------------------------------------")


def test_inner(x, idx, vis_id, phoc_def: PhocCompose, region_type: str, cand_scale: float, expand_policy: str) -> dict:
    try:
        canvas_ = svg_to_svg_canvas(x, region_type, scale=cand_scale, unify_policy=expand_policy)
        if canvas_ is None:
            # print(f"for some reason canvas is none???? {idx}")
            return None
        tested = phoc_def.test(canvas_)
        if tested is None:
            print(f"for some reason tested is none???? {idx}")
        return tested
    except Exception:
        print(f"testing failed against row: {idx} with vis id {vis_id}") #, with svg: {x}
        traceback.print_exc()
        return None


def build_batch_insert(x, idx, pid, index_name, retrieval_func, norm=None) -> str:
    try:
        compressed = compress_bitvectors(x, retrieval_func)
        if '.' in compressed:
            compressed.pop('.')
        if norm is not None:
            compressed["b_norm_factor"] = norm
        else:
            compressed["b_norm_factor"] = get_norm_factor(x)
        compressed["post_id"] = pid
        compressed_json = json.dumps(compressed, ensure_ascii=False)
        return """{{ "index" : {{ "_index": "{index_name}", "_id" : "{idx}" }} }}
{mapjson}""".format(idx=idx, mapjson=compressed_json, index_name=index_name)
    except Exception as e:
        print(f'3failed to build json id {idx} vectors {x}')
        traceback.print_exc()
        return ""


#destination_path = 'outputs/mappings/xy7o4_mapping.json'
#chunks_path = 'outputs/baselines/json_chunks/xy7o4arq13_2022/*'

#build_mapping_file(destination_path,chunks_path)