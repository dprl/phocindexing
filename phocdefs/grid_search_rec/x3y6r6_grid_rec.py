
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x3y6r6_grid_rec() -> PhocCompose:
    x=2
    y=5
    r=5
    vert_phoc = VertLinePhoc(0, 2)
    horz_phoc = HorzLinePhoc(0, 5)
    rec_phoc = RectanglePhoc(5)
    to_compose=[]
    if x>0:
        to_compose.append(vert_phoc)
    if y>0:
        to_compose.append(horz_phoc)
    if r>0:
        to_compose.append(rec_phoc)

    compose = PhocCompose(to_compose)

    return compose
            