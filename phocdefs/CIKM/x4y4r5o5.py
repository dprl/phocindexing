
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x4y4r5o5() -> PhocCompose:
    x=4
    y=4
    r=5
    o=5    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 3))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 3))
    if r>1:
        to_compose.append(RectanglePhoc(4))
    if o>1:
        to_compose.append(EllipsisPhoc(4))
    compose = PhocCompose(to_compose)

    return compose
                