from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x6y4r7_sp() -> PhocCompose:
    horz_phoc = HorzLinePhoc(0, 5, percentages=True)
    vert_phoc = VertLinePhoc(0, 3, percentages=True)
    rect_phoc = RectanglePhoc(6, percentages=True)
    compose = PhocCompose([
        horz_phoc,
        vert_phoc,
        rect_phoc
        ])
    return compose