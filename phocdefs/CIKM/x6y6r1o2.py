
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc


def get_x6y6r1o2() -> PhocCompose:
    x=6
    y=6
    r=1
    o=2    
    to_compose=[]
    if x>1:
        to_compose.append(VertLinePhoc(0, 5))
    if y>1:
        to_compose.append(HorzLinePhoc(0, 5))
    if r>1:
        to_compose.append(RectanglePhoc(0))
    if o>1:
        to_compose.append(EllipsisPhoc(1))
    compose = PhocCompose(to_compose)

    return compose
                