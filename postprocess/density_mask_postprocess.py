import math
import os
from concurrent.futures import ProcessPoolExecutor
from typing import List, Tuple
from tqdm.contrib.concurrent import process_map
from load_index import get_data_from_file
from postprocess.post_process_utils import zip_density_chunks, DensityChunk, zip_idf_chunks, IdfChunk, elementwise_add, \
    elementwise_mul
from process_utils import build_batch_insert


def get_density_mask(path_to_dir: str, workers: int) -> List[float]:
    chunks = zip_idf_chunks(path_to_dir, 1)
    print(f"density mask being extracted by {workers} workers")
    with ProcessPoolExecutor(max_workers=workers) as executor:
        chunk_results: List[Tuple[List, int]] = list(executor.map(get_density_mask_task, chunks))

    print(chunk_results[0])
    positional_actual_sum = [0.0] * len(chunk_results[0][0])
    positional_potential_sum = 0

    for chunk_result in chunk_results:
        observed_total = chunk_result[0]
        potential_total = chunk_result[1]

        positional_actual_sum = elementwise_add(positional_actual_sum, observed_total)
        positional_potential_sum += potential_total

    def idf(n, big_n):
        return max(math.log(big_n / (n + 1), 2), 0)

    return [idf(n, positional_potential_sum) for n in positional_actual_sum]


def get_density_mask_task(idf_chunk: IdfChunk) -> Tuple[List, int]:
    data = get_data_from_file(idf_chunk.full_path_to_input_file)
    formulas = data[1:len(data):2]
    total_actual_occurrences = None
    total_potential_occurrences = 0

    for formula in formulas:
        for symbol in formula:
            if symbol == 'b_norm_factor' or symbol == 'post_id':
                continue
            vect = formula[symbol]
            total_potential_occurrences += 1
            if total_actual_occurrences is not None:
                total_actual_occurrences = elementwise_add(total_actual_occurrences, vect)
            else:
                total_actual_occurrences = vect
    return total_actual_occurrences, total_potential_occurrences


def apply_density_mask(input_chunks_dir: str, output_chunks_dir: str, density_mask, workers: int, index_name: str):
    chunks = zip_density_chunks(input_chunks_dir, output_chunks_dir, density_mask, index_name)
    print(f"density mask being applied by {workers} workers")
    process_map(apply_density_mask_task, chunks, max_workers=workers)


def apply_density_mask_task(post_chunk: DensityChunk):
    data = get_data_from_file(post_chunk.full_path_to_input_file)
    file_name = os.path.split(post_chunk.full_path_to_input_file)[1]
    density_mask = post_chunk.densityMask
    with open(os.path.join(post_chunk.full_path_to_output_dir, file_name), 'w+', newline='') as batch_insert_file:
        meta = data[0:len(data):2]
        formulas = data[1:len(data):2]
        for d in zip(meta, formulas):
            index_data = d[0]
            formula_data = d[1]
            tested = formula_data
            for symbol in tested:
                if symbol == 'b_norm_factor' or symbol == 'post_id' or symbol == '__unifiedkeys__':
                    continue
                tested[symbol] = elementwise_mul(tested[symbol], density_mask)
            idx = index_data["index"]["_id"]
            post_id = formula_data["post_id"]
            index_name = post_chunk.index_name
            norm = formula_data["b_norm_factor"]
            retrieval_func = "symbol-percentage"
            batch_insert_str = build_batch_insert(tested, idx, post_id, index_name, retrieval_func, norm)
            batch_insert_file.write(batch_insert_str + "\n")
