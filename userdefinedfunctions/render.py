import logging
from pyspark.sql.functions import udf, col
from pyspark.sql.types import StringType
from anyphoc.canvas.converters.svgcanvas import render as ap_render


def render_inner(idx, x, url):
    try:

        return ap_render(x.strip(), url)
    except Exception as e:
        logger = logging.getLogger("broadcast")
        logger.warning(f"failed to render row {idx}  caused by exception {e}") #latex: {x}
        return None


def render_svg(url) -> udf:
    return udf(lambda idx, x: render_inner(idx, x, url), StringType())
