import csv
import glob
import html
import os
import time
from typing import List, Dict, Tuple
import re
import aiohttp
import asyncio
import sys
import bs4

errors = 0


async def render_formula_and_cache(path_to_data, render_url, path_to_cache_file):
    os.makedirs(os.path.split(path_to_cache_file)[0], exist_ok=True)
    if os.path.splitext(path_to_data)[1] == "":
        file_path = glob.glob(os.path.join(path_to_data, "*.tsv"))
        if len(file_path) == 0:
            file_path = glob.glob(os.path.join(path_to_data, "*.csv"))
    else:
        file_path = [path_to_data]

    with open(path_to_cache_file, 'w', newline='') as tsv_file:
        fieldnames = ['id', 'post_id', 'thread_id', 'type', 'visual_id', 'formula', 'rendered_formula']
        writer = csv.DictWriter(tsv_file, fieldnames=fieldnames, delimiter='\t', quotechar='"')
        count = 0
        count2 = 0
        count3 = 0
        batch_size = 128
        total_errors = 0
        for file in file_path:
            print(file)
            f = open(file, "r", encoding="utf-8")
            reader = csv.reader(f, delimiter='\t')
            next(reader)
            batch = []
            for line in reader:
                batch.append(line)
                if len(batch) >= batch_size:
                    d, batch_errors = await render_batch(batch, render_url)
                    total_errors += batch_errors
                    batch = []
                    if len(d) > 0:
                        writer.writerows(d)
                        count += batch_size
                        count2 += batch_size
                        count3 += batch_size
                if count2 > 1000:
                    print(f"rendered {count} formulas")
                    count2 = 0

                if count3 > 10_000:
                    await kill_servers()
                    count3 = 0
            if len(batch) > 0:
                d, batch_errors = await render_batch(batch, render_url)
                total_errors += batch_errors
                writer.writerows(d)
            f.close()

            await kill_servers()
        print(f"Files rendered with {total_errors} errors")


async def kill_servers():
    print("killing some node servers if they have too much memory")
    # this is a big hack. There is a memory leak in the mode server used to render the svgs.
    # eventually the server accumulate way to much memory and it becomes a problem. The solution here is to
    # make them kill themselves if they start to take on too much memory, so that they restart
    # without forcing a docker container to restart
    any_servers_killed = False
    for _ in range(1000):
        try:
            async with aiohttp.ClientSession() as session:

                await session.get('http://127.0.0.1:8045/killme')
        except:
            any_servers_killed = True
    if any_servers_killed:
        print("some servers killed")
        time.sleep(1)


async def render_batch(batch: List, render_url) -> Tuple[List[Dict], int]:
    try:
        session = aiohttp.ClientSession()
        coroutines = []
        batch_errors = 0
        for line in batch:
            if len(line) == 9:
                latex_string = line[8]
            else:
                latex_string = line[5]
            latex_string = html.unescape(latex_string)
            latex_string = latex_string.replace(r'\toggle', '')
            latex_string = latex_string.replace(r'\endtoggle', '')
            if "\\label" in latex_string:
                latex_string = re.sub(r"\\label(\{[^}]*})?", "", latex_string)
            if "\\tag" in latex_string:
                latex_string = re.sub(r"\\tag[^{]?({[^}]*})?", "", latex_string)
            if "\\text" in latex_string:
                latex_string = re.sub(r"\\text", "", latex_string)
            if r"%" in latex_string:
                latex_string = re.sub(r"(?<!\\\\)(%)", r"\\\1", latex_string)

            coroutines.append(session.post(render_url, json={'latex': latex_string}))

        responses = await asyncio.gather(*coroutines)
        final_dict_list = []
        for r, line in zip(responses, batch):
            status = r.status
            if status == 200:
                content = await r.content.read()
                content = content.decode('utf-8')
                soup = bs4.BeautifulSoup(content, 'lxml')

                if len(line) == 9:
                    formula = line[8]
                else:
                    formula = line[5]

                if soup.find('g', {'data-mml-node': 'merror'}) is None:
                    final_dict_list.append({
                        "id": line[0],
                        "post_id": line[1],
                        "thread_id": line[2],
                        "type": line[3],
                        "visual_id": line[4],
                        "formula": formula,
                        "rendered_formula": content
                    })
                else:
                    batch_errors += 1
                    print(f"error {line[0]}")
            else:
                print(status)
        await session.close()
        return final_dict_list, batch_errors
    except Exception as e:
        print(e)
        print("Mathjax ran out of heap space, retrying this batch")
        session.close()
        time.sleep(3)
        return await render_batch(batch, render_url)


def load_formulas_from_svg(spark, svg_path):
    return spark.read \
        .option("inferSchema", True) \
        .option("header", True) \
        .option("mode", "DROPMALFORMED") \
        .option("multiLine", True) \
        .option("escape", "\"") \
        .csv(svg_path, sep=r'\t')

