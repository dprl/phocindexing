import argparse
import ctypes
import dataclasses
import os
import time
from configparser import ConfigParser, ExtendedInterpolation
from typing import Callable

from tqdm import tqdm
from tqdm.contrib.concurrent import process_map

from phocdefs import phoc_defs
from io import StringIO
from configs.proconfigschema import pro_schema, validate_config_with_schema
from pro.loadcanvases import load_canvases
from userdefinedfunctions.build_batch_insert_pro import build_batch_insert_pro, build_batch_insert_inner_pro
from userdefinedfunctions.pro_test import pro_test


@dataclasses.dataclass
class YearChunk:
    tsv_path: str
    year: str
    get_phoc_comp: Callable
    final_json_chunks: str
    index_name: str
    cand_repr_string: str


def make_chunks(years, tsv_path, get_phoc_comp, final_json_chunks, index_name, cand_repr_string):
    return [YearChunk(
        tsv_path=tsv_path,
        year=year,
        get_phoc_comp=get_phoc_comp,
        final_json_chunks=final_json_chunks,
        index_name=index_name,
        cand_repr_string=cand_repr_string
    ) for year in years]


def process_year(chunk: YearChunk):
    print(f"processing PHOC data for year {chunk.year}")
    tsv_file_path = os.path.join(chunk.tsv_path, chunk.year, 'tsv')
    canvases = load_canvases(tsv_file_path, chunk.cand_repr_string)
    bulk_file = StringIO()
    print("testing canvases")
    for i, c in enumerate(tqdm(canvases)):
        phoc = chunk.get_phoc_comp()
        try:
            tested = phoc.test(c[3])
            bulk_file.write(
                build_batch_insert_inner_pro(tested, i, c[0], c[1], c[2], chunk.index_name, ctypes.c_int64))
            bulk_file.write("\n")
        except Exception as e:
            print(f"something went wrong with testing {chunk.year}")
            print(e)

    os.makedirs(os.path.split(chunk.final_json_chunks)[0], exist_ok=True)
    with open(chunk.final_json_chunks + f"bulk{chunk.year}.json", 'w') as fp:
        fp.write(bulk_file.getvalue())


def main(args):
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(args.config)
    validate_config_with_schema(config, pro_schema)
    years_file = config.get('meta', 'YEARS')
    tsv_path = config.get('spark', 'PRO_PATH')
    index_name = config.get('opensearch', 'INDEX_NAME')

    phoc_def_string = config.get('phoc', 'PHOC_DEF')
    cand_repr_string = config.get('phoc', 'REPR')

    final_json_chunks = config.get('metrics', 'JSON_CHUNKS_DIR')
    get_phoc_comp = phoc_defs[phoc_def_string]

    with open(years_file, "r") as years_file:
        years = years_file.readlines()
        years = [year.strip() for year in years]

    chunks = make_chunks(years, tsv_path, get_phoc_comp, final_json_chunks, index_name, cand_repr_string)

    print("working on graphics indexing with 30 workers")
    process_map(process_year, chunks, max_workers=30)
    '''for year in years:
        print(f"processing PHOC data for year {year}")
        tsv_file_path = os.path.join(tsv_path, year, 'tsv')
        canvases = load_canvases(tsv_file_path, cand_repr_string)
        bulk_file = StringIO()
        print("testing canvases")
        for i, c in enumerate(tqdm(canvases)):
            phoc = get_phoc_comp()
            try:
                tested = phoc.test(c[3])
                bulk_file.write(
                    build_batch_insert_inner_pro(tested, i, c[0], c[1], c[2], index_name, ctypes.c_int64))
                bulk_file.write("\n")
            except Exception as e:
                print(f"something went wrong with testing {year}")
                print(e)

        os.makedirs(os.path.split(final_json_chunks)[0], exist_ok=True)
        with open(final_json_chunks + "bulk.json", 'w') as fp:
            fp.write(bulk_file.getvalue())'''


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="../configs/sigir-demo/test.ini")
    parsed_args = parser.parse_args()

    start = time.time()
    main(parsed_args)
    end = time.time()
    print("time taken to produce bulk index file", end - start)
