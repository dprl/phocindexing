all:
	./bin/install

clean-full:
	./bin/clean-full

rebuild: clean-full all
default:
	./bin/runner configs/arqmath/default.ini

xy5:
	./bin/runner configs/arqmath/xy5.ini
xy7:
	./bin/runner configs/arqmath/xy7-small.ini
xy10:
	./bin/runner configs/arqmath/xy10-small.ini
x7y5:
	./bin/runner configs/arqmath/x7y5-small.ini
xyo5:
	./bin/runner configs/arqmath/xyo5-small.ini
xy7o5:
	./bin/runner configs/arqmath/xy7o5-small.ini

xy7o4-small:
	./bin/runner configs/arqmath/xy7o4-small.ini
xy7o5-idf:
	./bin/runner configs/arqmath/xy7o5-small-idf.ini
xy5-idf:
	./bin/runner configs/arqmath/xy5-small-idf.ini

xy5-full-vis-u:
	./bin/runner configs/arqmath/xy5-full-vis-unique.ini

xy7o4-full-vis-u:
	./bin/runner configs/arqmath/xy7o4-full-vis-unique.ini

xy7o4:
	./bin/runner configs/arqmath/xy7o4.ini

x7yo5:
	./bin/runner configs/arqmath/x7yo5-small.ini

xy5small:
	./bin/runner configs/arqmath/xy5-small.ini

o7:
	./bin/runner configs/arqmath/o7-small.ini

xy5-robin:
	./bin/runner configs/arqmath/xy5-robin.ini

xy7o4robin:
	./bin/runner configs/arqmath/xy7o4-robin.ini

xy7robin:
	./bin/runner configs/arqmath/xy7-robin.ini


