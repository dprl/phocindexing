from configparser import ConfigParser

from schema import Schema
from phocdefs import phoc_defs

pro_schema = Schema({
    "meta": {
        "EXP_NAMESPACE": str,
        "EXP_NAME": str,
        "YEARS": str
    },
    "spark": {
        "SOURCE": lambda s: s in ["PRO"],
        "PRO_PATH": str,
        "MAPPING_FILE": str
    },
    "phoc": {
        "PHOC_DEF": lambda pd: pd in phoc_defs.keys(),
        "REPR": lambda r: r in ["line", "centroid", "bbox"]
    },
    "opensearch": {
        "HOST": str,
        "PORT": str,
        "INDEX_NAME": str,
        "RETRIEVAL_FUNC": lambda rf: rf in ["idf", ""]
    },
    "metrics": {
        "PRE_POST_JSON_CHUNKS": str,
        "JSON_CHUNKS_DIR": str,
    }
})


def validate_config_with_schema(config: ConfigParser, schema: Schema):
    the_dict = {}
    for section in config.sections():
        the_dict[section] = {}
        for key, val in config.items(section):
            the_dict[section][key.upper()] = val
    schema.validate(the_dict)
