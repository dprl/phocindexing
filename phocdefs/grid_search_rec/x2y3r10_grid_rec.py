
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x2y3r10_grid_rec() -> PhocCompose:
    x=1
    y=2
    r=9
    vert_phoc = VertLinePhoc(0, 1)
    horz_phoc = HorzLinePhoc(0, 2)
    rec_phoc = RectanglePhoc(9)
    to_compose=[]
    if x>0:
        to_compose.append(vert_phoc)
    if y>0:
        to_compose.append(horz_phoc)
    if r>0:
        to_compose.append(rec_phoc)

    compose = PhocCompose(to_compose)

    return compose
            