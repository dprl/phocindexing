import json
import math
import random
import statistics
import matplotlib.pyplot as plt

from postprocess.post_process_utils import elementwise_add


def idf_map_histogram(idf_map, bins=100, name=None):
    idf_magnitudes = [idf_map[m] for m in idf_map]

    lowest_symbols = [m for m in idf_map if idf_map[m] < .59]
    print(f"Lowest Symbols with lowest IDF: {lowest_symbols}")
    num_below_one = [m for m in idf_map if idf_map[m] < 1]
    print(f"Symbols with IDF below 1: {num_below_one}")

    above_15 = [m for m in idf_map if idf_map[m] > 15.358]
    print(f"top {len(above_15)} Symbols with top IDF of 15.358: {above_15}")
    print(f"IDF mean: {statistics.mean(idf_magnitudes)}, std: {statistics.stdev(idf_magnitudes)},"
          f" min: {min(idf_magnitudes)}, max {max(idf_magnitudes)}")
    plt.hist(idf_magnitudes, color='blue', edgecolor='black', bins=bins)
    # plt.title('Frequency of IDF Values')
    plt.xlabel('IDF Score', size=16)
    plt.ylabel('Number of Symbols', size=16)
    if name is not None:
        name = "_" + name
    plt.savefig(f"idf_hist{name}.pdf", format="pdf")
    plt.show()


# WARNING this code will only work with R9 configuration
def pos_idf_map_histogram(pos_idf_map, level_amounts=None, bins=100, name=None):
    if level_amounts is None:
        level_amounts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    magnitudes_per_level = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: []}


    for symbol in pos_idf_map:
        s = pos_idf_map[symbol]
        magnitudes_per_level[1].extend([s[0]])
        magnitudes_per_level[2].extend(s[1:3])
        magnitudes_per_level[3].extend(s[3:6])
        magnitudes_per_level[4].extend(s[6:10])
        magnitudes_per_level[5].extend(s[10:15])
        magnitudes_per_level[6].extend(s[15:21])
        magnitudes_per_level[7].extend(s[21:28])
        magnitudes_per_level[8].extend(s[28:36])
        magnitudes_per_level[9].extend(s[36:45])
        magnitudes_per_level[10].extend(s[45:56])

    print(magnitudes_per_level[1])
    print(f"level 1 mean: {statistics.mean(magnitudes_per_level[1])},"
          f" std: 0, min: {min(magnitudes_per_level[1])}, max {max(magnitudes_per_level[1])}")
    for i in range(2, 11):
        print(f"level {i} mean: {statistics.mean(magnitudes_per_level[i])},"
              f" std: {statistics.stdev(magnitudes_per_level[i])},"
              f" min: {min(magnitudes_per_level[i])}, max {max(magnitudes_per_level[i])}")

    for i, bin_width in enumerate(level_amounts):
        ax = plt.subplot(4, 3, i + 1)

        ax.hist(magnitudes_per_level[i+1], bins=bins, color='blue', edgecolor='black')
        ax.set_title('Region Based IDF Scores at Level %d' % bin_width, size=10)
        ax.set_xlabel('IDF Score', size=10)
        ax.set_ylabel('Number of Symbols', size=10)

    plt.tight_layout()
    if name is not None:
        name = "_" + name
    plt.savefig(f"pos_hist_all.pdf{name}", format="pdf")
    plt.show()

    for i, bin_width in enumerate([1, 10]):
        # ax = plt.subplot(1, 2, i + 1)

        plt.hist(magnitudes_per_level[i+1], bins=bins, color='blue', edgecolor='black')
        # plt.set_title('Region Based IDF Scores at Level %d' % bin_width, size=10)
        plt.tick_params(axis='both', which='major', labelsize=16)
        plt.xlabel('IDF Score', size=16)
        plt.ylabel('Number of Symbols', size=16)
        plt.tight_layout()
        fig = plt.gcf()
        fig.set_size_inches(5.7, 5)
        fig.savefig(f"pos_hist_{i}.pdf", format="pdf")
        plt.show()


def density_histogram(density_mask, name=None):
    plt.stairs(density_mask, list(range(len(density_mask) + 1)), color='black', fill=False, edgecolor='black')
    # plt.hist(density_mask, color='blue', edgecolor='black', bins=bins)
    plt.xlabel('Region (r)', size=16)
    plt.ylabel('IDF Score', size=16)
    if name is not None:
        name = "_" + name
    plt.savefig(f"density_hist{name}.pdf", format="pdf")
    plt.show()


def symbol_idf_histogram(density_mask, name=None):
    plt.stairs(density_mask, list(range(len(density_mask) + 1)), color='black', fill=False, edgecolor='black')
    # plt.hist(density_mask, color='blue', edgecolor='black', bins=bins)
    plt.axis([-3, 58, 0, 19])
    # ax.set_title(f'IDF Scores for Symbol {name}', size=10)

    plt.xlabel('Region (r)', size=18)
    plt.ylabel('IDF Score', size=18)
    plt.tick_params(axis='both', which='major', labelsize=16)
    plt.tight_layout()
    fig = plt.gcf()
    fig.set_size_inches(5.7, 5)
    fig.savefig(f"symbol_hist_{name}.pdf", format="pdf", dpi=256)
    plt.show()


def symbol_statistics_regardless_of_level(pos_idf_map, name=None):

    std = 0
    m = 0
    avg_vec = [0]*55
    for symbol in pos_idf_map:
        sym_vec = pos_idf_map[symbol]
        m += statistics.mean(sym_vec)
        std += statistics.stdev(sym_vec)
        avg_vec = elementwise_add(avg_vec, sym_vec)

    avg_vec = [val/len(pos_idf_map) for val in avg_vec]
    plt.stairs(avg_vec, list(range(len(avg_vec) + 1)), color='black', fill=False, edgecolor='black')
    # plt.hist(density_mask, color='blue', edgecolor='black', bins=bins)
    plt.xlabel('Region (r)', size=16)
    plt.ylabel('Average IDF Score', size=16)
    if name is not None:
        name = "_" + name
    plt.savefig(f"average_val_hist{name}.pdf", format="pdf")
    plt.show()
    print(f"average mean for regions, relative to a given symbol {m/len(pos_idf_map)}")
    print(f"average stdev for regions, relative to a given symbol {std/len(pos_idf_map)}")

    equals_symbol = pos_idf_map["="]
    symbol_idf_histogram(equals_symbol, name="=")
    print(f"Standard deviation of Region Based IDF scores for '=' {statistics.stdev(equals_symbol)}")

    does_not_contain_symbol = pos_idf_map["⋫"]
    print(f"Standard deviation of Region Based IDF scores for '⋫' {statistics.stdev(does_not_contain_symbol)}")
    symbol_idf_histogram(does_not_contain_symbol, name="nott")
    # ax = plt.subplot(1, 3, 3)
    # plus_symbol = pos_idf_map["+"]
    # print(f"Standard deviation of Region Based IDF scores for '+' {statistics.stdev(plus_symbol)}")
    # symbol_idf_histogram(plus_symbol, ax, name="+")


if __name__ == "__main__":

    with open('/home/mlang/Documents/dprl/phocindexing/outputs/sigir-8/idf_map/r9idf/idf_map.json') as json_file:

        data = json.load(json_file)
        idf_map_histogram(data, 25, "")

    # with open('/home/mlang/Documents/dprl/phocindexing/outputs/sigir-8/idf_map/r9idflb/idf_map.json') as json_file:
        # pass
        # data = json.load(json_file)
        # idf_map_histogram(data, 250, name="934")

    with open('/home/mlang/Documents/dprl/phocindexing/outputs/sigir-8/idf_map/r9posidf/idf_map.json') as json_file:
        data = json.load(json_file)
        pos_idf_map_histogram(data, bins=25)
        symbol_statistics_regardless_of_level(data)



