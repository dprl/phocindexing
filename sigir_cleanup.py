import argparse
import time
from typing import Dict

from opensearchpy import OpenSearch
import os
import glob
from io import StringIO
import json


if __name__ == '__main__':
    auth = ('admin', 'admin')
    client = OpenSearch(
        hosts=[{'host': 'localhost', 'port': '9200'}],
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=auth,
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    parser = argparse.ArgumentParser()
    parser.add_argument('--one', type=bool, default=False)
    parser.add_argument('--two', type=bool, default=False)
    parser.add_argument('--three', type=bool, default=False)
    parser.add_argument('--four', type=bool, default=False)
    parser.add_argument('--five', type=bool, default=False)
    parser.add_argument('--six', type=bool, default=False)
    parser.add_argument('--seven', type=bool, default=False)
    parser.add_argument('--eight', type=bool, default=False)

    parsed_args = parser.parse_args()

    phase_one = ["xy1visu", "xy1visuidf", "xy5visu", "xy7o4visu"]
    # we aren't doing xy7o4 here
    phase_two = ["xy5bbox", "xy5centroid"]
    # we just use the xy5 and xy7o4 visu sets here
    phase_three = []
    # s stands for "scale"
    phase_four = ["xy5s6", "xy5s7", "xy5s8", "xy5s9", "xy7o4s6", "xy7o4s7", "xy7o4s8", "xy7o4s9"]
    # phase 5 just uses the same indices as phases four and one
    phase_five = []
    phase_six = []
    phase_seven = ["xy5even", "xy5odd", "xy5skip2", "xy5skiptop", "xy7o4even", "xy7o4odd", "xy7o4skip2", "xy7o4skiptop", "xy10even", "xy10odd"]
    phase_eight = \
        ["x5y5o6idf", "x5y5o6idfrelu", "x5y5o6posidf", "x5y5o6posidfrelu", "x5y5o6density"
         "x7y5o6idf", "x7y5o6idfrelu", "x7y5o6posidf", "x7y5o6posidfrelu", "x7y5o6density"]

    if parsed_args.one:
        for ind in phase_one:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.two:
        for ind in phase_two:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.three:
        for ind in phase_three:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.four:
        for ind in phase_four:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.five:
        for ind in phase_five:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.six:
        for ind in phase_six:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.seven:
        for ind in phase_seven:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

    if parsed_args.eight:
        for ind in phase_eight:
            print(f"deleting index ${ind}")
            if client.indices.exists(index=ind):
                print(client.indices.delete(index=ind))

