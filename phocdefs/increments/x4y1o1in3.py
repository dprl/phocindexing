
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x4y1o1in3() -> PhocCompose:

    vert_phoc = VertLinePhoc(0, 4, increments=3)
    horz_phoc = HorzLinePhoc(0, 1, increments=3)
    ellip_phoc = EllipsisPhoc(1, increments=3)
    compose = PhocCompose([
        vert_phoc,
        horz_phoc,
        ellip_phoc
        ])
    return compose
    