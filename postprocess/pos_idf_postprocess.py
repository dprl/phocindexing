import math
import os
from typing import List, Dict, Tuple

from tqdm.contrib.concurrent import process_map
from concurrent.futures import ProcessPoolExecutor
from load_index import get_data_from_file
from postprocess.post_process_utils import PostChunk, zip_post_chunks, elementwise_add, elementwise_mul, IdfChunk, \
    zip_idf_chunks
from process_utils import build_batch_insert


def get_pos_idf_map(path_to_dir: str, workers, lower_bound: int) -> Dict[str, List[float]]:
    chunks = zip_idf_chunks(path_to_dir, 1)
    print(f"pos-idf map being extracted by {workers} workers")
    with ProcessPoolExecutor(max_workers=workers) as executor:
        chunk_results: List[Tuple] = list(executor.map(get_pos_idf_map_task, chunks))
    # process_map(get_pos_idf_map_task, chunks, max_workers=workers, initializer=init_worker, initargs=(q,))
    pos_idf_dict: Dict[str, List[float]] = dict()
    positional_sum = [0]*len(chunk_results[0][1])

    for chunk_result in chunk_results:
        positional_sum = elementwise_add(positional_sum, chunk_result[1])
        chunk_pos_idf_dict = chunk_result[0]
        for potential_new_symbol in chunk_pos_idf_dict:
            if potential_new_symbol == 'b_norm_factor' \
                    or potential_new_symbol == 'post_id'\
                    or potential_new_symbol == '__unifiedkeys__':
                continue
            symbol_count_vector = pos_idf_dict.get(potential_new_symbol)
            if symbol_count_vector is not None:
                pos_idf_dict[potential_new_symbol] = \
                    elementwise_add(symbol_count_vector, chunk_pos_idf_dict[potential_new_symbol])
            else:
                pos_idf_dict[potential_new_symbol] = chunk_pos_idf_dict[potential_new_symbol]

    for symbol in pos_idf_dict:
        if symbol == 'b_norm_factor' \
                or symbol == 'post_id' \
                or symbol == '__unifiedkeys__':
            continue
        symbol_vect = pos_idf_dict[symbol]
        for pos in range(len(symbol_vect)):
            # where idf actually calculated
            sum_per_symbol: float = symbol_vect[pos]
            sum_total: float = positional_sum[pos]
            idf_term = max(math.log(sum_total / (sum_per_symbol + 1)), lower_bound)
            symbol_vect[pos] = idf_term

    return pos_idf_dict


def get_pos_idf_map_task(idf_chunk: IdfChunk):
    pos_idf_dict: Dict[str, List[float]] = dict()

    data = get_data_from_file(idf_chunk.full_path_to_input_file)
    formulas = data[1:len(data):2]
    for formula in formulas:
        for symbol in formula:
            if symbol == 'b_norm_factor' or symbol == 'post_id':
                continue
            vect = formula[symbol]
            old_vect = pos_idf_dict.get(symbol)
            if old_vect is not None:
                pos_idf_dict[symbol] = elementwise_add(old_vect, vect)
            else:
                pos_idf_dict[symbol] = vect

    first_sym = list(pos_idf_dict.keys())

    positional_sum = [0] * len(pos_idf_dict[first_sym[0]])

    for symbol in pos_idf_dict:
        positional_sum = elementwise_add(positional_sum, pos_idf_dict[symbol])

    return pos_idf_dict, positional_sum


def apply_pos_idf_map(input_chunks_dir: str, output_chunks_dir: str, idf_map, workers: int, index_name: str):
    chunks = zip_post_chunks(input_chunks_dir, output_chunks_dir, idf_map, index_name)
    print(f"pos-idf map being applied by {workers} workers")
    process_map(apply_pos_idf_map_task, chunks, max_workers=workers)


def apply_pos_idf_map_task(post_chunk: PostChunk):
    data = get_data_from_file(post_chunk.full_path_to_input_file)
    file_name = os.path.split(post_chunk.full_path_to_input_file)[1]
    pos_idf_map = post_chunk.map
    with open(os.path.join(post_chunk.full_path_to_output_dir, file_name), 'w+', newline='') as batch_insert_file:
        meta = data[0:len(data):2]
        formulas = data[1:len(data):2]
        for d in zip(meta, formulas):
            index_data = d[0]
            formula_data = d[1]
            tested = formula_data
            for symbol in tested:
                if symbol == 'b_norm_factor' or symbol == 'post_id' or symbol == '__unifiedkeys__':
                    continue
                vector_idf_term = pos_idf_map[symbol]
                tested[symbol] = elementwise_mul(tested[symbol], vector_idf_term)
            idx = index_data["index"]["_id"]
            post_id = formula_data["post_id"]
            index_name = post_chunk.index_name
            norm = formula_data["b_norm_factor"]
            retrieval_func = "symbol-percentage"
            batch_insert_str = build_batch_insert(tested, idx, post_id, index_name, retrieval_func, norm)
            batch_insert_file.write(batch_insert_str + "\n")