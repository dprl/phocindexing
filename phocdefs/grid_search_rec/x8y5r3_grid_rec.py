
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x8y5r3_grid_rec() -> PhocCompose:
    x=7
    y=4
    r=2
    vert_phoc = VertLinePhoc(0, 7)
    horz_phoc = HorzLinePhoc(0, 4)
    rec_phoc = RectanglePhoc(2)
    to_compose=[]
    if x>0:
        to_compose.append(vert_phoc)
    if y>0:
        to_compose.append(horz_phoc)
    if r>0:
        to_compose.append(rec_phoc)

    compose = PhocCompose(to_compose)

    return compose
            