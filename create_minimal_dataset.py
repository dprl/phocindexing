import os

from pyspark.sql import SparkSession
import argparse

from pyspark.sql.functions import regexp_replace, col
from pyspark.sql.types import StructType, StringType, IntegerType, StructField


def main(spark, args):
    tsv_df = spark.read\
        .option("inferSchema", True)\
        .option("header", True)\
        .option("mode", "DROPMALFORMED")\
        .option("multiLine", True)\
        .csv(args.tsvdir, sep=r'\t')
    print(tsv_df.show())

    schema = StructType([
        StructField("q_id", StringType(), True),
        StructField("zero", IntegerType(), True),
        StructField("formula_id", IntegerType(), True),
        StructField("relevance", IntegerType(), True),
        ])

    qrel_df = spark.read\
        .schema(schema)\
        .option("header", False)\
        .option("mode", "DROPMALFORMED")\
        .option("multiLine", True)\
        .csv(args.qrel, sep=r'\t')
    qrel_df.show()

    joined_df = tsv_df.join(qrel_df, tsv_df.id == qrel_df.formula_id, 'left_semi')
    joined_df.show()

    joined_df_clean = joined_df\
        .withColumnRenamed("formula", "formulaOld")\
        .withColumn("formula", regexp_replace(col("formulaOld"), "[\n\r]", " "))\
        .drop("formulaOld")
    joined_df_clean\
        .write\
        .mode('overwrite')\
        .csv(os.path.split(args.outfile)[0], sep=r'\t', header=True)
    os.system(f"cat {os.path.split(args.outfile)[0]}/p* > {args.outfile}")
    os.system(f"rm {os.path.split(args.outfile)[0]}/part-*")
    os.system(f"rm {os.path.split(args.outfile)[0]}/.part-*")
    os.system(f"rm {os.path.split(args.outfile)[0]}/._SUCCESS.crc")
    os.system(f"rm {os.path.split(args.outfile)[0]}/_SUCCESS")


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--tsvdir", default="inputs/latex_representation_V2")
    parser.add_argument("-q", "--qrel", default="inputs/qrels/qrel_task2_2021_formula_id_test.tsv")
    parser.add_argument('-o', "--outfile", default="inputs/latex_from_qrels/only_evaluated_inputs.tsv")
    parsed_args = parser.parse_args()

    spark_session = SparkSession.builder\
        .appName("anyphoc")\
        .master("local[*]")\
        .getOrCreate()
    main(spark_session, parsed_args)
