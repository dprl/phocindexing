
from phocdefs.build_compose import build_compose


for x in range(12):
    for y in range(12):
        for r in range(12):
            for o in range(12):
                if build_compose(x, y, r, o).explain()[1] > 64 or (x == y and y == r and r == o and o == 0):
                    continue

                name_get = f"get_x{x}y{y}r{r}o{o}"
                print(f"   \"x{x}y{y}r{r}o{o}\":{name_get},")
                #print(f"from phocdefs.CIKM.x{x}y{y}r{r}o{o} import {name_get}")
                #print(f"./bin/runner configs/sigir-configs/9-grid-search/x{x}y{y}r{r}o{o}_grid_rec.ini")
