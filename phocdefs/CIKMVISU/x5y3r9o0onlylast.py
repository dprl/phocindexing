import os.path

from anyphoc.canvas.converters.svgcanvas import svg_to_svg_canvas
from anyphoc.phoc.ellipsis.ellipsisphoc import EllipsisPhoc
from anyphoc.phoc.horzline.horzlinephoc import HorzLinePhoc
from anyphoc.phoc.phoccompose import PhocCompose
from anyphoc.phoc.rectangle.rectanglephoc import RectanglePhoc
from anyphoc.phoc.vertline.vertlinephoc import VertLinePhoc


def get_x5y3r9o0_only_last() -> PhocCompose:

    to_compose = [
        VertLinePhoc(0, 4, starting_level=4),
        HorzLinePhoc(0, 2, starting_level=2),
        RectanglePhoc(8, starting_level=8),
    ]

    compose = PhocCompose(to_compose, skip_first_level=True)

    return compose


if __name__ == "__main__":
    with open(os.path.join("phocdefs", "CIKMVISU", "h_dom.svg")) as f:
        svg = f.read()

    canvas_ = svg_to_svg_canvas(svg, region_type="line")
    get_x5y3r9o0_only_last().build_html(canvas_, os.path.join("phocdefs", "CIKMVISU"), "ok")
