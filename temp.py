from phocdefs.build_compose import build_compose

count_configs = 0
domain = [x for x in range(1,12,2)]
domain.insert(0,0)
for x in domain:
    for y in domain:
        for r in domain:
            for o in domain:
                if build_compose(x, y, r, o).explain()[1] > 64 or (x == y and y == r and r == o and o == 0):
                    continue

                print(f"x{x}y{y}r{r}o{o} configs:{count_configs}")
                count_configs += 1